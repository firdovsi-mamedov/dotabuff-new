
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/** Components for Admin Panel */

Vue.component('users-component', require('./components/adminpanel/users/Index.vue').default);
Vue.component('users-create-component', require('./components/adminpanel/users/Create.vue').default);
Vue.component('users-edit-component', require('./components/adminpanel/users/Edit.vue').default);


Vue.component('admins-component', require('./components/adminpanel/admins/Index.vue').default);
Vue.component('admins-create-component', require('./components/adminpanel/admins/Create.vue').default);
Vue.component('admins-edit-component', require('./components/adminpanel/admins/Edit.vue').default);


Vue.component('accounts-select-component', require('./components/adminpanel/accounts/Select.vue').default);
Vue.component('accounts-component', require('./components/adminpanel/accounts/Index.vue').default);

Vue.component('accounts-create-mmr-component', require('./components/adminpanel/accounts/types/create/MMR.vue').default);
Vue.component('accounts-create-tbd-component', require('./components/adminpanel/accounts/types/create/TBD.vue').default);
Vue.component('accounts-create-hours-component', require('./components/adminpanel/accounts/types/create/Hours.vue').default);

Vue.component('accounts-edit-mmr-component', require('./components/adminpanel/accounts/types/edit/MMR.vue').default);
Vue.component('accounts-edit-tbd-component', require('./components/adminpanel/accounts/types/edit/TBD.vue').default);
Vue.component('accounts-edit-hours-component', require('./components/adminpanel/accounts/types/edit/Hours.vue').default);

Vue.component('accounts-edit-component', require('./components/adminpanel/accounts/Edit.vue').default);


Vue.component('faqs-component', require('./components/adminpanel/faqs/Index.vue').default);
Vue.component('faqs-create-component', require('./components/adminpanel/faqs/Create.vue').default);
Vue.component('faqs-edit-component', require('./components/adminpanel/faqs/Edit.vue').default);


Vue.component('rules-component', require('./components/adminpanel/rules/Index.vue').default);
Vue.component('rules-create-component', require('./components/adminpanel/rules/Create.vue').default);
Vue.component('rules-edit-component', require('./components/adminpanel/rules/Edit.vue').default);


Vue.component('languages-select-component', require('./components/adminpanel/languages/Select.vue').default);
Vue.component('languages-component', require('./components/adminpanel/languages/Index.vue').default);
Vue.component('languages-create-component', require('./components/adminpanel/languages/Create.vue').default);
Vue.component('languages-edit-component', require('./components/adminpanel/languages/Edit.vue').default);


Vue.component('reviews-component', require('./components/adminpanel/reviews/Index.vue').default);
Vue.component('reviews-create-component', require('./components/adminpanel/reviews/Create.vue').default);
Vue.component('reviews-edit-component', require('./components/adminpanel/reviews/Edit.vue').default);


Vue.component('settings-component', require('./components/adminpanel/settings/Edit.vue').default);





/** Components for Website Main Page */

Vue.component('website-accounts-mmr-component', require('./components/website/accounts/Mmr.vue').default);
Vue.component('website-accounts-tbd-component', require('./components/website/accounts/Tbd.vue').default);
Vue.component('website-accounts-hours-component', require('./components/website/accounts/Hours.vue').default);

Vue.component('website-boosters-component', require('./components/website/BoostersComponent').default);
Vue.component('website-chat-component', require('./components/website/ChatComponent').default);

Vue.component('vue-slider-component', require('./components/website/vue-slider/Slider').default);
Vue.component('wishlist-component', require('./components/website/WishlistComponent').default);



/** Components for Profile Page */

Vue.component('solobooster-component', require('./components/profile/booster/SoloBooster').default);
Vue.component('calibre-component', require('./components/profile/booster/Calibre').default);
Vue.component('otmit-component', require('./components/profile/booster/Otmit').default);
Vue.component('learning-component', require('./components/profile/booster/Learning').default);
Vue.component('profile-message-component', require('./components/profile/MessageComponent').default);
Vue.component('profile-work-component', require('./components/profile/WorkComponent').default);

Vue.component('vue-dropzone-add-portfolio', require('./components/profile/AddPortfolio').default);



Vue.mixin({
    methods: {
        route: route
    }
});


const app = new Vue({
    el: '#app'
});
