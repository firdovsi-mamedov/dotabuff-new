export default {
    data(){
        return {

        }
    },
    methods:{
        closeSuccess(){
            this.success = false;
        },
        closeError(){
            this.error = false
        },
        messages(status = null){
            if(status === 200){
                this.success = true;
                setTimeout(()=>{
                    this.success = false;
                }, 4000);
            }else{
                this.error = true;
                setTimeout(()=>{
                    this.errors = [];
                    this.error = false;
                }, 4000);
            }
        }

    }
}
