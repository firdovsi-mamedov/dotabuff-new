<!DOCTYPE html>
<html>
<head>
	<title>Title</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>

	<div id="app">
		@yield('content')
	</div>

	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>