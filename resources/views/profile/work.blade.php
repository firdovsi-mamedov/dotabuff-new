@extends('profile.layouts.master')



@section('content')


<section>
    <div class="container">
        <div class="card chat-room">
            <div class="card-body">
            <profile-work-component :conversation_id="{{$conversation_id ?? 0}}" :sender="{{Auth::user()}}" :role="{{$role}}"></profile-work-component>
            </div>
        </div>

    </div>
</section>
</div>

@endsection
