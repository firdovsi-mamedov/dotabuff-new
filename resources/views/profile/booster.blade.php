@extends('profile.layouts.master')


@section('content')

  <section class="booster-page">
      <form class="" action="" method="post">
        @csrf

        <div class="container">
            <div class="subscribe">
                <div>Ваша подписка:не оплачено <a href="{{ route("profile.plans") }}">Выбрать тариф</a></div>
                <div>Ваши услуги не будут отображатся в каталоге пока не <a href="{{ route("profile.plans") }}">купите подписку</a></div>
            </div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#solo" role="tab" aria-controls="home" aria-selected="true">Solo Boost</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#party" role="tab" aria-controls="profile" aria-selected="false">Party Boost</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#calibre" role="tab" aria-controls="contact" aria-selected="false">Калибровка</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#lp" role="tab" aria-controls="contact" aria-selected="false">Отмыть ЛП</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#learning" role="tab" aria-controls="contact" aria-selected="false">Обучение</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade active show" id="solo" role="tabpanel" aria-labelledby="profile-tab">
                    @role("booster")
                        <solobooster-component :user="{{Auth::user()}}"></solobooster-component>
                    @endrole
                </div>
                <div class="tab-pane fade" id="party" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <button class="btn btn-success">Заказать</button>
                    <div></div>
                    <br>
                    <div class="chat-booster">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#chat" role="tab" aria-controls="home" aria-selected="true">Переписка с Бустером</a>
                            </li>
                            <li class="nav-item checkbox-enable">
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" name="enable_notification" value="1">
                                        <span class="lever"></span>
                                        Включить оповещение
                                    </label>
                                </div>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="chat" role="tabpanel" aria-labelledby="home-tab">
                                <br>
                                @if($user->status == 0)
                                <div class="mesgs">
                                    <div class="msg_history">
                                        <div class="incoming_msg">
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <h5 class="user-msg">Dimonhelper</h5>
                                                    <p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации</p>
                                                    <span class="time_date"> 15.11.2018    |    20:32</span></div>
                                            </div>
                                        </div>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <h5 class="user-msg">Dimonhelper</h5>
                                                <p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации</p>
                                                <span class="time_date"> 15.11.2018    |    20:32</span></div>
                                        </div>
                                    </div>
                                    <div class="type_msg">
                                        <div class="input_msg_write">
                                            <textarea class="write_msg" placeholder="Ваше сообщение" /></textarea>
                                            <button class="msg_send_btn btn btn-success waves-light"type="button">Отправить</button>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <p>Бустер занят заказом, его услуги нельзя оплатить пока он не выполнит взятый заказ.</p>
                                <p>Если хотитк сразу узнать когда он освободиться, <span>оставьте номер</span> для получение SMS</p>
                                <div class="sms-form">
                                    <div class="card">
                                        <div class="card-header">
                                            SMS оповещение
                                        </div>
                                        <div class="card-body">
                                            <p class="card-text">Укажите ваш номер мобильного, чтобы узнать когда освободиться Бустер</p>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Ваш номер">
                                            </div>
                                            <button class="btn btn-primary">Ок</button>
                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="calibre" role="tabpanel" aria-labelledby="contact-tab">
                @role("booster")
                    <calibre-component :user="{{Auth::user()}}"></calibre-component>
                @endrole
                </div>
                @role("booster")
                <div class="tab-pane fade" id="lp" role="tabpanel" aria-labelledby="contact-tab">
                  <otmit-component :user="{{Auth::user()}}"></otmit-component>
                </div>
                @endrole
                @role("booster")
                <div class="tab-pane fade" id="learning" role="tabpanel" aria-labelledby="contact-tab">
                  <learning-component :user="{{Auth::user()}}"></learning-component>
                </div>
                @endrole
            </div>
        </div>

      </form>
    </section>

    <br>
    <br>
    <br>


    </div>

@stop

@push('swipper')

    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 5,
            spaceBetween: 30,
            slidesPerGroup: 1,
            loop: true,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>

@endpush
