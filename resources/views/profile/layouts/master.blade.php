<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Аккаунты Дота 2, Ранги и MMR</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/uploads/favicon.ico" type="image/x-icon">

        @routes
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/profile/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/profile/css/mdb.min.css" rel="stylesheet">
    <!-- Swipper Css Slider -->
    <link rel="stylesheet" href="/profile/css/swiper.min.css">
    <script src="/profile/js/swiper.min.js"></script>
    <!--Font Awesome 5 icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <!-- Your custom styles (optional) -->
    <link href="/profile/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>

<body>

  <div id="app">
      <header>
          <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
              <div class="container">
                  <a class="navbar-brand" href="/">digg</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent-4">

                      <ul class="navbar-nav ml-auto">

                          <li class="nav-item">
                          <a class="nav-link waves-effect waves-light" href="{{route('profile.index')}}">
                                  <i class="fa fa-gear"></i> Профиль</a>
                          </li>
                          <li class="nav-item ">
                              <a class="nav-link waves-effect waves-light" id="navbarDropdownMenuLink-4" >
                                  <i class="fa fa-user"></i> Мой счет  <span> руб</span></a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
      </header>
      <br>

      <section>
          <div class="container">
              <nav class="nav">
                <a class="nav-link active" href="{{route('profile.deals')}}"><i class="far fa-handshake"></i> Сделки</a>
                <a class="nav-link" href="{{route('profile.booster')}}"><i class="fas fa-briefcase"></i> Услуги</a>
                <a class="nav-link" href="{{route('profile.message')}}"><i class="far fa-comments"></i> Сообщение ({{$messagesCount}})</a>
                <a class="nav-link" href="{{route('profile.work')}}"><i class="fas fa-rocket"></i> Заказы (1)</a>
              </nav>
          </div>
      </section>
      <br>

  @yield('content')


</div>
</div>
</div>
<!-- /Start your project here-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="/profile/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/profile/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/profile/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/profile/js/mdb.min.js"></script>
<script type="text/javascript" src="/profile/js/script.js"></script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>

<script>
    // popovers Initialization
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>
@stack('swippers')


</body>

</html>
