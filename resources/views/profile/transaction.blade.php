@extends('profile.layouts.master')


@section('content')


<section>
    <div class="container">
        <h4 class="mb-3 mt-3">На счёту: 55 рублей (<small>Минимальная сумма вывода: 500 рублей</small>)</h4>
        <div class="input-group" style="width: 450px;">
            <select class="custom-select"  id="inputGroupSelect04" aria-label="Example select with button addon">
                <option selected>Выбирайте...</option>
                  <option value="1">Qiwi  </option>
                  <option value="2">Webmoney </option>
                  <option value="3">MC/Visa </option>
                  <option value="4">PayPal </option>
            </select>
            <div class="input-group-append">
                <button class="btn btn-primary waves-light btn-sm mt-0 pb-2" style="font-size: 14px;;" type="button">Вывести</button>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <h4 class="mb-3 mt-3">Запросы на вывод</h4>
        <table class="table table-moderators">

            <tbody>
                <tr>
                    <td><a href="deal.php" class="a-link"></a></td>
                    <td>{{date('d-m-Y')}}</td>
                    <td>Продажа буст Solo MMR</td>
                    <td>Denis</td>
                    <td>fu9607</td>
                    <td>50 р.</td>
                    <td>
                         <a href="javascript:void(0)" class="btn btn-success btn-2" data-toggle="popover"
                           title="Детали операции" data-content="Дата: 12.10.2018, Время: 15:00, Способ: QIWI, id: 1223223" >OK</a>
                         {{--<div class="btn-group">--}}
                             {{--<button type="button" class="btn btn-primary btn-2 grey">--}}
                                 {{--В обработке--}}
                             {{--</button>--}}
                         {{--</div>--}}
                          {{--<a href="#" class="btn btn-danger red btn-2">Отклонён</a>--}}
                    </td>
                </tr>

            </tbody>

        </table>
        <br>
        <button class="btn btn-info">Загрузить ещё</button>

    </div>
    <br>
</section>

@stop
