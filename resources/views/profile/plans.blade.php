@extends('profile.layouts.master')



@section('content')


<section>
    <div class="container">
        <h6 class="mb-3 mt-3">Ваша подписка: не оплачено (<a href="#">Выбрать тариф</a>)</h6>
        <h6 class="mb-3 mt-3">Ваши услуги не будут отображаться в каталоге пока не <a href="#">купите подписку</a></h6>

    </div>
</section>
<section class="my-5">
    <div class="container">
        <div class="">
            <h2 class="text-center">Тарифы для solo бустеров </h2>
            <p class="text-center">На тарифах для Solo бустеров можно принять только один активный заказ. На командном тарифе - несколько оформлений. Вы получете больше заказов. Это тариф выгоднее и дешевле с партнером по буст. </p>
        </div>
        <div class="row">
          @foreach(range(1,4) as $item)
            <div class="col-sm-3 col-md-3">
                <div class="option">
                    <div class="option-title">
                        <h3>{{$item}} Дней</h3>
                        <span>{{$item}}</span>
                    </div>
                    <div class="option-service">
                        <span class="period"><strong>Available</strong></span>
                    </div>
                    <div class="option-service">
                        <span class="period">Rating</span>
                    </div>
                    <div class="option-service">
                        <span class="period">Catalog</span>
                    </div>
                    <div class="option-service">
                        <span class="period">Economy</span>
                    </div>

                </div>
                <div class="option-list">
                    <button type="button" class="btn btn-success big btn-buy">
                      <span class="cost">{{$item}} РУБ</span>
                      <span class="txt">{{$item}}</span>
                    </button>
                </div>
            </div>
          @endforeach
        </div><!-- /.row -->
    </div><!-- /.container -->

</section>
<br>
<br>
<section class="my-5">
    <div class="container">
        <div class="">
            <h2 class="text-center">Тарифы для Команд и Сервисов </h2>
            <p class="text-center">На тарифах для Solo бустеров можно принять только один активный заказ. На командном тарифе - несколько оформлений. Вы получете больше заказов. Это тариф выгоднее и дешевле с партнером по буст. </p>
        </div>
        <div class="row justify-content-md-center">
            @foreach(range(1, 4) as $item)

            <div class="col-sm-3 col-md-3">
                <div class="option">
                    <div class="option-title">
                        <h3>{{$item}} дней</h3>
                        <span>{{$item}}</span>
                    </div>

                    <div class="option-service">
                        <span class="period"><strong>Available</strong></span>
                    </div>
                    <div class="option-service">
                        <span class="period">Rating</span>
                    </div>
                    <div class="option-service">
                        <span class="period">Catalog</span>
                    </div>
                    <div class="option-service">
                        <span class="period">Economy</span>
                    </div>

                </div>
                <div class="option-list">
                    <a href="/profile/methods" type="button" class="btn btn-success big btn-buy">
                      <span class="cost">{{$item}} РУБ</span>
                      <span class="txt">{{$item}}</span>
                    </a>
                </div>
            </div>
            @endforeach

        </div><!-- /.row -->
    </div><!-- /.container -->

</section>


</div>


@stop
