@extends('profile.layouts.master')



@section('content')
    <section>
        <div class="container">
            <div class="card chat-room">
                <div class="card-body">
                    <!-- Grid column -->
                    <profile-message-component :conversation_id="{{$conversation_id ?? 0}}" :sender="{{Auth::user()}}"></profile-message-component>
                    <!-- Grid column -->
                </div>
            </div>

        </div>
    </section>
    </div>
@endsection
