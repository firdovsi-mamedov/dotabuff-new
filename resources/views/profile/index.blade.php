@extends('profile.layouts.master')


@section('content')



    <section>
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Профиль</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false">Платежные данные</a>
                </li>
                @role("booster")
                <li class="nav-item">
                    <a class="nav-link" id="portfolio-tab" data-toggle="tab" href="#portfolio" role="tab" aria-controls="payment" aria-selected="false">Портфолио</a>
                </li>
                <li class="mt-2">
                    <a class="ml-5">
                        <i class="far fa-user"></i> Смотреть профиль на сайте
                    </a>
                </li>
                @endrole
            </ul>
            <form class="" action="{{route("profile.index.update")}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="home-tab">
                    <div class="col-md-6  offset-md-0  toppad" >
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title user-information">
                                    <h5>Ваше фото</h5>
                                    @if($user->image !== null)
                                        <div class="user-round-img" style="background-image: url({{"/uploads/avatars/".$user->image}})"></div>
                                    @else
                                        <div class="user-round-img" style="background-image: url(/uploads/avatars/default.png)"></div>
                                    @endif
                                    <div class="image-select">
                                      <input type="file" name="image" value="{{$user->image}}">
                                    </div>
                                </div>

                                <br>
                                <table class="table table-user-information ">
                                    <tbody>
                                      <tr>
                                          <td>Статус</td>
                                          <td>
                                              <select
                                                  icon="status-icon"
                                                  name="user_status"
                                                  label=""
                                                  class="form-control">
                                                  <option value="0" @if($user->user_status == 0) selected @endif>Offline</option>
                                                  <option value="1" @if($user->user_status == 1) selected @endif>Online</option>
                                            </select>
                                          </td>
                                      </tr>
                                    <tr>
                                        <td>Nickname</td>
                                        <td>
                                            <input
                                                icon="nickname-icon"
                                                name="nickname"
                                                type="text"
                                                label=""
                                                class="form-control"
                                                value="{{$user->nickname}}"
                                                focus
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>
                                            <input
                                                icon="email-icon"
                                                name="email"
                                                type="email"
                                                label=""
                                                class="form-control"
                                                value="{{$user->email}}"
                                                focus
                                            />
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Пароль</td>
                                        <td>
                                            <input
                                                icon="password-icon"
                                                name="password"
                                                type="password"
                                                label=""
                                                class="form-control"
                                                value=""
                                                focus
                                            />
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Телефон</td>
                                        <td>
                                            <input
                                                icon="phone-icon"
                                                name="phone"
                                                type="tel"
                                                label=""
                                                class="form-control"
                                                value="{{$user->phone}}"
                                                focus
                                            />
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                                <br>
                                <div class="pad-tbl">
                                    <div>
                                        <input type="checkbox" name="email_status" id="email-push" @if($user->email_status == 1) checked @endif> <label for="email-push">Включить оповещение email</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" name="browser_status" id="browser-push" @if($user->browser_status == 1) checked @endif> <label for="browser-push">Включить оповещение в браузере</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" name="sms_status" id="sms-push" @if($user->sms_status == 1) checked @endif> <label for="sms-push">Включить SMS оповещение</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="save-changes" name="button">Сохранить изменение</button>
                    </div>
                </div>
                <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="col-md-6  offset-md-0  toppad" >
                        <div class="card">
                            <div class="card-body">
                            <table class="table table-user-information ">
                                <tbody>

                                <tr>
                                    <td>QIWI</td>
                                    <td>
                                        <input
                                            icon="nickname-icon"
                                            name="wallet_qiwi"
                                            type="text"
                                            label=""
                                            class="form-control"
                                            value="{{$user->wallet_qiwi}}"
                                            focus
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>WebMoney</td>
                                    <td>
                                        <input
                                            icon="email-icon"
                                            name="wallet_webmoney"
                                            type="text"
                                            label=""
                                            class="form-control"
                                            value="{{$user->wallet_webmoney}}"
                                            focus
                                        />
                                    </td>
                                </tr>
                                <tr>

                                    <td>MC/Visa</td>
                                    <td>
                                        <input
                                            icon="password-icon"
                                            name="wallet_visa"
                                            type="text"
                                            label=""
                                            class="form-control"
                                            value="{{$user->wallet_visa}}"
                                            focus
                                        />
                                    </td>
                                </tr>
                                <tr>

                                    <td>PayPal</td>
                                    <td>
                                        <input
                                            icon="phone-icon"
                                            name="wallet_paypal"
                                            type="text"
                                            label=""
                                            class="form-control"
                                            value="{{$user->wallet_paypal}}"
                                            focus
                                        />
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @role("booster")
                <div class="tab-pane fade" id="portfolio" role="tabpanel" aria-labelledby="portfolio-tab">
                    <div class="col-md-12  offset-md-0  toppad" >
                        <div class="card">
                            <div class="card-body">
                              <div>Портфолио стимулирует пользователей сделать заказ у Вас.
                                Добавляйте серии побед и записывайте хайлайты. </div>
                              <div>Мы можем создать хайлайты из ваших игр,для этого пишите id матчей на почту admin@dotarating.com</div>
                              <div>В теме письма укажите: хайлайты и ваш никнейм</div>
                              <br>
                              <!--Start Tab Pills-->
                              <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#image" role="tab" aria-controls="image" aria-selected="true">Изображение</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#youtube" role="tab" aria-controls="youtube" aria-selected="false">Ссылка на YouTube</a>
                                </li>

                              </ul>

                              <br>
                            </div>

                            <vue-dropzone-add-portfolio :user="{{Auth::user() ?? 1}}"></vue-dropzone-add-portfolio>
                        </div>
                    </div>
                </div>
            @endrole
            </div>
        </div>
    </section>
</div>

@stop

@push('swippers')

<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 5,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        fade: {
           crossFade:true
         }
    });
    $('#portfolio-tab').click(function(){
      reinitSwiper(swiper)
    });
    function reinitSwiper(swiper) {
        setTimeout(function () {
            swiper.update();
        }, 200);
    }
</script>

@endpush
