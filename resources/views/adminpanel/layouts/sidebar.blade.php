<div class="left main-sidebar">

    <div class="sidebar-inner leftscroll">

        <div id="sidebar-menu">

            <ul>

                <li class="submenu">
                    <a class="active" href="{{ route('admin.dashboard') }}"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                </li>

                <li class="submenu">
                    <a href="{{ route('admin.accounts.select') }}"><i class="fa fa-fw fa-area-chart"></i><span> Аккаунты </span> </a>
                </li>

                <li class="submenu">
                    <a href="{{ route('admin.deals.index') }}"><i class="fa fa-fw fa-table"></i> <span> Сделки </span> </a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fa fa-fw fa-tv"></i> <span> Настройки </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" style="display: block;">
                        <li><a href="{{ route('admin.settings.edit') }}">Основные</a></li>
                        <li><a href="{{ route('admin.settings.translate') }}">Translate</a></li>
                    </ul>
                </li>

                <li class="submenu">
                    <a href="{{ route('admin.faqs.index') }}"><i class="fa fa-fw fa-file-text-o"></i> <span> FAQ's </span> </a>
                </li>
                <li class="submenu">
                    <a href="{{ route('admin.rules.index') }}"><i class="fa fa-fw fa-file-text-o"></i> <span> Правила </span> </a>
                </li>
                 <li class="submenu">
                    <a href="{{ route('admin.languages.select') }}"><i class="fa fa-fw fa-file-text-o"></i> <span> Языки </span></a>
                </li>

                <li class="submenu">
                    <a href="{{ route('admin.users.index') }}"><i class="fa fa-fw fa-th"></i> <span> Пользователи </span></a>
                </li>

                <li class="submenu">
                    <a href="{{ route('admin.admins.index') }}"><i class="fa fa-fw fa-image"></i> <span> Модераторы </span></a>
                </li>
                <li class="submenu">
                    <a href="{{ route('admin.reviews.index') }}"><i class="fa fa-fw fa-image"></i> <span> Отзывы </span></a>
                </li>


            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>

</div>
