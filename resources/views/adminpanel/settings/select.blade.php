@extends('adminpanel.layouts.master')

@section('content')

    @include('adminpanel.layouts.sidebar')

    <div class="content-page">

        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">



                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card mb-12">
                            <div class="card-header">
                                <h3><i class="fa fa-users"></i> Staff details</h3>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non luctus metus. Vivamus fermentum ultricies orci sit amet sollicitudin.
                            </div>


                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col">
                                                <div class="dataTables_length" id="example1_length"><label>Show entries
                                                        <select name="example1_length" aria-controls="example1" class="form-control form-control-sm">
                                                            <option value="10">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div id="example1_filter" class="dataTables_filter"><label>
                                                        Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example1"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col text-right">
                                        <a href="#" class="btn btn-success btn-sm">Добавить</a>
                                    </div>
                                </div>

                                <table id="example1" class="table table-bordered table-responsive-xl table-hover display">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Тип</th>
                                        <th>Dotabuff</th>
                                        <th>Пользователи</th>
                                        <th>Дата</th>
                                        <th>E-mail</th>
                                        <th>Наличии</th>
                                        <th>Цена</th>
                                        <th>Включить</th>
                                        <th>Редактировать/Удалить</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Tiger Nixon</td>
                                        <td>61</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>Tiger Nixon</td>
                                        <td>Accountant</td>
                                        <td>61</td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-sm">Редактировать</a>
                                            <button class="btn btn-danger btn-sm">Удалить</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Tiger Nixon</td>
                                        <td>61</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>Tiger Nixon</td>
                                        <td>Accountant</td>
                                        <td>61</td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-sm">Редактировать</a>
                                            <button class="btn btn-danger btn-sm">Удалить</button>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div><!-- end card-->
                    </div>


                </div>



            </div>
            <!-- END container-fluid -->

        </div>
        <!-- END content -->

    </div>
@endsection
