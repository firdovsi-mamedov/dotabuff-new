@extends('adminpanel.layouts.master')

@section('content')

    @include('adminpanel.layouts.sidebar')

    <div class="content-page">

        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        @if($type == 1)
                            <accounts-create-mmr-component :account="{{$account}}"></accounts-create-mmr-component>
                        @elseif($type == 2)
                            <accounts-create-tbd-component :account="{{$account}}"></accounts-create-tbd-component>
                        @else
                            <accounts-create-tbd-component :account="{{$account}}"></accounts-create-tbd-component>
                        @endif
                        <!-- end card-->
                    </div>

                </div>
                <!-- end row -->

            </div>
            <!-- END container-fluid -->

        </div>
        <!-- END content -->

    </div>
@endsection
