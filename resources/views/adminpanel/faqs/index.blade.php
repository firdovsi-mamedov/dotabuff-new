@extends('adminpanel.layouts.master')

@section('content')

    @include('adminpanel.layouts.sidebar')

    <div class="content-page">

        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">



                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <faqs-component></faqs-component>
                        <!-- end card-->
                    </div>


                </div>



            </div>
            <!-- END container-fluid -->

        </div>
        <!-- END content -->

    </div>
@endsection
