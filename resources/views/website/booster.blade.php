@extends('website.layouts.master')


@section('content')


    <section>
        <div class="container">
            <div class="booster-data">
                <ul class="nav">
                    <li class="boost-main">
                        <div class="boost-title">Denis</div>
                        @if($booster->image !== null)
                        <div class="boost-img" style="background-image: url(/uploads/avatars/{{$booster->image}})">
                            <div class="online-stat ">Online</div>
                            <div class="boost-status">Был вчера</div>
                        </div>
                        @else
                        <div class="boost-img" style="background-image: url(/uploads/avatars/default.png)">
                            <div class="online-stat ">Offline</div>
                            <div class="boost-status">Был вчера</div>
                        </div>
                        @endif
                    </li>
                    <li>
                        <div class="boost-title">
                            TOP <i class="far fa-question-circle"></i>
                        </div>
                        <div class="boost-img_section">
                            <span>12</span>

                        </div>
                    </li>
                    <li>
                        <div class="boost-title">
                            Rate <i class="far fa-question-circle"></i>
                        </div>
                        <div class="boost-img_section">
                            <span>12%</span>
                        </div>
                    </li>
                    <li>
                        <div class="boost-title">
                            Заказов
                        </div>
                        <div class="boost-img_section">
                            <span>55</span>
                        </div>
                    </li>
                    <li>
                        <div class="boost-title">
                            MMR
                        </div>
                        <div class="boost-img_section">
                            <span>55</span>
                        </div>
                    </li>
                    <li>
                        <div class="boost-title">
                            Стаж <i class="far fa-question-circle"></i>
                        </div>
                        <div class="boost-img_section">
                            <span>5</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <br>
    <br>

    <section class="portfolio">
        <div class="container">
            <h2>Портфолио</h2>
            <!-- Swiper -->
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($portfolio as $image)
                    <div class="swiper-slide" style="background-image:url(/uploads/portfolio/{{$image->image}})"></div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

            <!-- Swiper JS -->
        </div>
    </section>
    <br>
    <section class="booster-page">
        <div class="container">
            <h2>Услуги бустера</h2>
            <br>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#solo" role="tab" aria-controls="home" aria-selected="true">Solo Boost</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#party" role="tab" aria-controls="profile" aria-selected="false">Party Boost</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#calibre" role="tab" aria-controls="contact" aria-selected="false">Калибровка</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#lp" role="tab" aria-controls="contact" aria-selected="false">Отмыть ЛП</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#learning" role="tab" aria-controls="contact" aria-selected="false">Обучение</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="party" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <vue-slider-component :user_id="{{$id}}"></vue-slider-component>
                    <br>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <form class="" action="" method="post">
                        @csrf
                        <input type="hidden" name="type" value="2">
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-success">Заказать</button>
                    </form>
                    <div></div>
                    <br>

                </div>
                <div class="tab-pane fade active show" id="solo" role="tabpanel" aria-labelledby="profile-tab">
                    <br>
                    <vue-slider-component :user_id="{{$id}}"></vue-slider-component>
                    <br>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <form class="" action="" method="post">
                        @csrf
                        <input type="hidden" name="type" value="1">
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-success">Заказать</button>
                    </form>
                    <div></div>
                    <br>
                    <div class="clearfix"></div>

                </div>
                <div class="tab-pane fade" id="calibre" role="tabpanel" aria-labelledby="contact-tab">
                    <br>
                    <vue-slider-component :user_id="{{$id}}"></vue-slider-component>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <form class="" action="" method="post">
                        @csrf
                        <input type="hidden" name="type" value="3">
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-success">Заказать</button>
                    </form>
                    <div></div>
                    <br>

                    <div class="clearfix"></div>

                </div>
                <div class="tab-pane fade" id="lp" role="tabpanel" aria-labelledby="contact-tab">
                    <br>
                    <vue-slider-component :user_id="{{$id}}"></vue-slider-component>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <form class="" action="" method="post">
                        @csrf
                        <input type="hidden" name="type" value="4">
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-success">Заказать</button>
                    </form>
                    <div></div>
                    <br>

                </div>
                <div class="tab-pane fade" id="learning" role="tabpanel" aria-labelledby="contact-tab">
                    <br>
                    <vue-slider-component :user_id="{{$id}}"></vue-slider-component>
                    <p>Укажите Ваш текущий MMR и мжелаемый. Мы покажем стоимость услуг бустера, обращаем внимание что допустимая погрешность при заказе и выполнение услуг составляет +-10 MMR </p>
                    <h5>Цена услуги: <span class="cost">2445 рублей</span></h5>
                    <form class="" action="" method="post">
                        @csrf
                        <input type="hidden" name="type" value="5">
                        <input type="hidden" name="id" value="">
                        <button type="submit" class="btn btn-success">Заказать</button>
                    </form>
                    <div></div>
                    <br>
                    <div class="clearfix"></div>
                </div>
                @auth
                    <website-chat-component :receiver_id="{{$id}}" :sender="{{$user ?? 1}}"></website-chat-component>
                @endauth
            </div>
        </div>
    </section>

</div>

@stop

@push('swipper')

<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 5,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>

@endpush
