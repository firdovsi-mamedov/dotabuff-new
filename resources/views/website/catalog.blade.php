@extends('website.layouts.master')

@section('content')

    <div class="content">
        <section>
            <div class="container">
                <h2 class="text-center">{{__('dota_accounts_with_warranty')}}</h2>
                <p class="text-center">{{__('forgot_about_register')}}</p>

                <div class="stat-top">
                    <ul class="d-flex nav">
                        <li>
                            <a href="#">{{$boosters}}</a>
                            <span>{{__('total_boosters')}}</span>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-android"></i> 500</a>
                            <span>{{__('free_boosters')}}</span>
                        </li>
                        <li>
                            <a href="#">5442</a>
                            <span>{{__('total_orders_completed')}}</span>
                        </li>
                    </ul>
                </div>

                <br>
            </div>
        </section>
        <br>
        <br>
        <section class="page-booster">
           <website-boosters-component></website-boosters-component>
        </section>
        <br>
        <br>
        <section>
            <div class="container">
                <h3 class="text-center">{{__('why_need_buy_overlvl')}}</h3>
                <div class="col-list d-flex">
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="fas fa-shield-alt"></i>
                        </div>
                        <h5 class="text-center">{{__('real_guarantee')}}</h5>
                        <p class="text-center">{{__('take_attention_most_websites')}}</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="fas fa-check-square"></i>
                        </div>
                        <h5 class="text-center">{{__('reliable_accounts')}}</h5>
                        <p class="text-center">{{__('pay_only_from_this_page')}}</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="far fa-clock"></i>
                        </div>
                        <h5 class="text-center">{{__('online_support')}}</h5>
                        <p class="text-center">{{__('online_support_all_the_time')}}</p>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <section class="faq-section">
            <div class="container">
                <h3 class="faq text-center">FAQ</h3>
                <div class="accordion" id="accordionExample">
                    @foreach($faqs as $faq)
                        @php
                            $title = "title_".$lang;
                            $content = "content_".$lang;
                        @endphp
                    <div class="card">
                        <div class="card-header" id="heading{{$faq->id}}" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                            <h5 class="mb-2 mt-2">
                                {{ $faq->$title }}
                            </h5>
                        </div>

                        <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                            <div class="card-body">
                               {{ $faq->$content }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="text-center">
                    <p><strong>Покупателю:</strong> Забудьте про риски нарваться на продавца - кидалу и про гарантов. Overlvl даёт  финансовую гарантию на любой товар и услугу.</p>
                    <p><strong>Продавцу:</strong> Больше не нужна репутация и отзывы, чтобы просто продать свой аккаунт или услугу. <br> Избавь себя от общение с кидалами и поисков гарантов. Мы рекламируем Ваш товар, находим покупателя, даём ему гарантию.</p>
                </div>
            </div>
        </section>
        <br>

    </div>
</div>

@stop
