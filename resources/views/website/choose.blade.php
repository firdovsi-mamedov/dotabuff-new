@extends('website.layouts.master')


@section('content')

    <br>
    <br>
    <section>
        <div class="container">
            <form action="{{route('choose.buy')}}" method="POST">
                @csrf
            <input type="hidden" name="account_id" value="{{$id}}">
            <input type="hidden" name="type" value="{{$type}}">
            <h3 class="text-center">Ваш заказ: {{$account->name}}</h3>
            <h4 class="text-center">Сумма: {{$account->price}} руб.</h4>
            <h4 class="text-center">Укажите Email, на который будут высланы данные от аккаунта:</h4>
            <br>
            <div class="text-center">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" id="email" name="email" class="form-control email-for-acc">
                </div>
            </div>
            <br>
            <h4 class="text-center">Выберите способ оплаты</h4>
            <br>
            <div class="payments m-auto row">
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_03.png)" for="first">
                    <input type="radio" name="payment" checked id="first" value="1">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_05.png)" for="second">
                    <input type="radio" name="payment" id="second" value="2">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_07.png)" for="third">
                     <input type="radio" name="payment" id="third" value="3">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_09.png)" for="forth">
                     <input type="radio" name="payment" id="forth" value="4">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_11.png)" for="fifth">
                     <input type="radio" name="payment" id="fifth" value="5">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_18.png)" for="sixes">
                     <input type="radio" name="payment" id="sixes" value="6">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_19.png)" for="seventh">
                     <input type="radio" name="payment" id="seventh" value="7">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_20.png)" for="eights">
                     <input type="radio" name="payment" id="eights" value="8">
                </label>
                <label class="method" style="background-image:url(/frontend/img/wallets/payment_21.png)" for="nines">
                     <input type="radio" name="payment" id="nines" value="9">
                </label>
            </div>
            <div class="text-center mt-3"><button class="btn btn-info">Оплатить заказ</button></div>
        </form>
            <!-- Default form register -->
        </div>
    </section>
    <br>




</div>

@stop
