@extends('website.layouts.master')


@section('content')


    <section class="register-form">
        <div class="container">
          <div class="card">

              <div class="card-body mx-4 mt-4 ">
                <form class="" action="{{ route('register') }}" method="post">
                  @csrf


                    <h3 class="pl-1">Регистрация</h3>
                    <!--Body-->

                    <div class="form-group col s12 pl-1">
                        <label for="login">Name</label>
                        <input id="login" type="text" name="name" class="form-control form-3"  value="{{old('name')}}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group col s12 pl-1">
                        <label for="email">E-mail</label>
                        <input id="email" type="email" name="email" class="form-control form-3"  value="{{old('email')}}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group col s12 pl-1">
                        <label for="password">Password</label>
                        <input id="password" type="password" name="password" class="form-control form-3">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>
                    <input type="hidden" name="role" value="booster">
                    <div class="form-group col s12 pl-1">
                        <label for="password-confirm">Confirm Password</label>
                        <input id="password-confirm" type="password" name="password_confirmation" class="form-control form-3">
                    </div>
                    <div class="form-group col s12 pl-1">
                        <label for="wallet">Кошелёк</label>
                        <select id="wallet" name="wallet" class="form-control form-3">
                            <option value="1">Qiwi</option>
                            <option value="2">WebMoney</option>
                        </select>
                    </div>

                    <h3 class="text-center">Правила</h3>
                    <div class="accordion" id="accordionExample">
                        @foreach($rules as $rule)
                        <div class="card">
                            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="mb-2 mt-2">
                                    {{ $rule->title_ru }}
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{ $rule->content_ru }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <br>
                    <!--Grid row-->
                    <div class="d-flex mb-4" style="justify-content: center">
                    <!--Grid column-->
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-rounded z-depth-1a waves-effect waves-light">Зарегистрироваться</button>
                        </div>
                    <!--Grid column-->

                    </div>
                    <!--Grid row-->
                  </form>
                </div>

            </div>
        </div>
    </section>
</div>

@stop
