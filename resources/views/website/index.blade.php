@extends('website.layouts.master')

@section('content')



    <div class="content">
        <section>
            <div class="container">
                <h2 class="text-center">{{__('dota_accounts_with_warranty')}}</h2>
                <p class="text-center">{{__('forgot_about_register')}}</p>

                <div class="stat-top">
                    <ul class="d-flex nav">
                        <li>
                            <a href="#">{{$counts["all"]}}</a>
                            <span>{{__('dota_accounts')}}</span>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-android"></i> {{$counts["soldMonth"]}}</a>
                            <span>{{__('month_sold')}}</span>
                        </li>
                        <li>
                            <a href="#">{{$counts["soldAll"]}}</a>
                            <span>{{__('sold_all_the_time')}}</span>
                        </li>
                    </ul>
                </div>
                <br>
                <div class="accounts-four">
                    <ul class="nav d-flex accounts-with-img">
                        <li>
                            <a href="{{ route("index.account.type", ["mmr"])}} "><img src="/uploads/favicon.ico" alt=""> Аккаунты ММР</a>
                        </li>
                        <li>
                            <a href="{{ route("index.account.type", ["tbd"])}} "><img src="/uploads/favicon.ico" alt=""> Аккаунты TBD</a>
                        </li>
                        <li>
                            <a href="{{ route("index.account.type", ["hours"])}} "><img src="/uploads/favicon.ico" alt=""> Аккаунты с часами</a>
                        </li>
                        <li>
                            <a href="{{ route("index.services") }}"><img src="/uploads/favicon.ico" alt=""> Услуги</a>
                        </li>
                    </ul>
                </div>
                <br>
                <div class="mr-counts">
                    <ul class="nav d-flex">
                        <li>
                            <a href="#">2000+ MMR <i class="fas fa-chevron-right"></i></a>
                            <span>145</span>
                        </li>
                        <li>
                            <a href="#">3000+ MMR <i class="fas fa-chevron-right"></i></a>
                            <span>8</span>
                        </li>
                        <li>
                            <a href="#">4000+ MMR <i class="fas fa-chevron-right"></i></a>
                            <span>12</span>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <br>
        <br>
        <section>
            <div class="container">
                    @if($type === 1)
                        <website-accounts-mmr-component :account="{{$account}}" :user="{{Auth::user() ?? 1}}"></website-accounts-mmr-component>
                    @elseif($type === 2)
                        <website-accounts-tbd-component :account="{{$account}}" :user="{{Auth::user() ?? 1}}"></website-accounts-tbd-component>
                    @elseif($type === 3)
                        <website-accounts-hours-component :account="{{$account}}" :user="{{Auth::user() ?? 1}}"></website-accounts-hours-component>
                    @else
                        <website-accounts-mmr-component :account="{{$account}}" :user="{{Auth::user() ?? 1}} "></website-accounts-mmr-component>
                    @endif
            </div>
        </section>
        <br>
        <br>
        <section>
            <div class="container">
                <h2 class="text-center">{{__('index.why_us')}}</h2>
                <div class="col-list d-flex">
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="fas fa-shield-alt"></i>
                        </div>
                        <h5 class="text-center">{{__('real_guarantee')}}</h5>
                        <p class="text-center">{{__('take_attention_most_websites')}}</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="fas fa-check-square"></i>
                        </div>
                        <h5 class="text-center">{{__('reliable_accounts')}}</h5>
                        <p class="text-center">{{__('pay_only_from_this_page')}}</p>
                    </div>
                    <div class="col-lg-4">
                        <div class="icon-why text-center">
                            <i class="far fa-clock"></i>
                        </div>
                        <h5 class="text-center">{{__('online_support')}}</h5>
                        <p class="text-center">{{__('online_support_all_the_time')}}</p>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <section class="faq-section">
            <div class="container">
                <h2 class="faq text-center">FAQ</h2>
                <div class="accordion" id="accordionExample">
                    @foreach($faqs as $faq)
                        @php
                            $title = "title_".$lang;
                            $content = "content_".$lang;
                        @endphp
                    <div class="card">
                        <div class="card-header" id="heading{{$faq->id}}" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                            <h5 class="mb-2 mt-2">
                                {{ $faq->$title }}
                            </h5>
                        </div>

                        <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                            <div class="card-body">
                               {{ $faq->$content }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="text-center">
                    <p><strong>Покупателю:</strong> Забудьте про риски нарваться на продавца - кидалу и про гарантов. Overlvl даёт  финансовую гарантию на любой товар и услугу.</p>
                    <p><strong>Продавцу:</strong> Больше не нужна репутация и отзывы, чтобы <span class="text-danger">просто продать</span> свой аккаунт или услугу. <br> Избавь себя от общение с кидалами и поисков гарантов. Мы рекламируем Ваш товар, находим покупателя, даём ему гарантию.</p>
                </div>
            </div>
        </section>
        <br>
        <section>
            <div class="container">
                <div class="reviews">
                    <h2 class="text-center review-txt">Last Reviews</h2>
                    <div class="row">
                        @foreach($reviews as $review)
                        <div class="col-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><a href="#" class="btn btn-primary">{{$review->title}}</a>  <small>{{date_format($review->created_at, "d-m-Y")}}</small></h5>
                                    <p class="card-text">{{$review->description}}</p>
                                    <a href="#" class="">{{$review->name}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <br>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <h3 class="text-center">Blog</h3>
                        <div class="news-section row">

                            <div class="news-main col-6 d-flex">
                                <div class="new-image" style="background-image: url()"></div>
                                <div class="new-txt">
                                    Name
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <h3 class="text-center">Orders</h3>
                        <div class="order-list">
                            <div class="order-part d-flex">
                                <div class="order-img" style="background-image: url(img/order.jpg)"></div>
                                <div class="order-desc">
                                    <h5><a href="#">Заказ #3422</a></h5>
                                    <p>Буст solo MMR</p>
                                    <p>От 2350 MMR - до 3725</p>
                                </div>
                                <div class="order-bar">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="order-part d-flex">
                                <div class="order-img" style="background-image: url(img/order.jpg)"></div>
                                <div class="order-desc">
                                    <h5><a href="#">Заказ #3422</a></h5>
                                    <p>Буст solo MMR</p>
                                    <p>От 2350 MMR - до 3725</p>
                                </div>
                                <div class="order-bar">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="order-part d-flex">
                                <div class="order-img" style="background-image: url(img/order.jpg)"></div>
                                <div class="order-desc">
                                    <h5><a href="#">Заказ #3422</a></h5>
                                    <p>Буст solo MMR</p>
                                    <p>От 2350 MMR - до 3725</p>
                                </div>
                                <div class="order-bar">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


@stop
