<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Аккаунты Дота 2, Ранги и MMR</title>
    @routes
    <!-- Font Awesome -->
    <link rel="icon" href="/uploads/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('website/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('website/css/mdb.min.css') }}" rel="stylesheet">
    <!-- Swipper Css Slider -->
    <link rel="stylesheet" href="/website/css/swiper.min.css">
    <script src="{{ asset('website/js/swiper.min.js') }}"></script>
    <!--Font Awesome 5 icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <!--JQuery UI Slider Range-->
    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}



    <!-- Your custom styles (optional) -->
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> -->
</head>

<body>

  <div id="app">
      <header>
          <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
              <div class="container">
                  <a class="navbar-brand" href="{{ route('index') }}">Dota</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                      <div>
                          <ul class="navbar-nav">
                              <li class="nav-item active">
                                  <a class="nav-link waves-light pl-3 pr-3 rounded" mdbWavesEffect href="{{ route('index') }}">{{__("index.catalog")}} <span class="sr-only">(current)</span></a>
                              </li>
                              <li class="nav-item ">
                                  <a class="nav-link waves-light pl-3 pr-3 rounded" mdbWavesEffect href="{{ route('index.services') }}">{{__("index.service")}}</a>
                              </li>

                          </ul>
                      </div>
                      <ul class="navbar-nav ml-auto align-items-center">
                          <li class="nav-item active">

                                  <ul class="nav">
                                      <li>
                                          <form action="{{route("index.language", ["language"=>"en"])}}" method="POST">
                                              @csrf
                                              <button type="submit">EN</button>
                                          </form>
                                      </li>
                                      <li>
                                          <form action="{{route("index.language", ["language"=>"ru"])}}"  method="POST">
                                              @csrf
                                              <button type="submit">RU</button>
                                          </form>
                                      </li>
                                  </ul>

                          </li>
                          @guest
                          <li class="nav-item">
                            <a href="/login" class="nav-link waves-effect waves-light">
                             Login
                            </a>
                          </li>
                              <li class="nav-item">
                                  <a href="/register" class="nav-link waves-effect waves-light">
                                      register
                                  </a>
                              </li>
                          @endguest
                          @auth
                          <li class="nav-item">
                            <a href="{{route('profile.message')}}" class="nav-link waves-effect waves-light">
                              <i class="far fa-comments"></i>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a href="{{route('profile.work')}}" class="nav-link waves-effect waves-light">
                              <i class="fas fa-rocket"></i>
                            </a>
                          </li>
                          <li class="nav-item drop">
                            <a href="#" class="nav-link waves-effect waves-light" id="wishlist">
                              <i class="far fa-star"></i>
                            </a>
                            <div class="dropdown-wishlist">
                              <div class="wishlist-header">
                                    {{__('wishlist')}}
                              </div>

                                <wishlist-component :user="{{ Auth::user() ?? 1}}"></wishlist-component>

                            </div>
                          </li>

                          <li class="nav-item">
                            <a href="#" onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();" class="nav-link waves-effect waves-light">
                              <i class="fas fa-sign-out-alt"></i>
                            </a>
                          </li>


                          <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fa fa-user"></i>  </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                                  <a class="dropdown-item waves-effect waves-light" href="{{ route("profile.index") }}">Профиль</a>
                                  <a class="dropdown-item waves-effect waves-light" href="#">Мои товары</a>
                                  @role("booster")
                                  <a class="dropdown-item waves-effect waves-light" href="{{ asset("profile.booster") }}">Мои услуги</a>
                                  @endrole
                                  <a class="dropdown-item waves-effect waves-light" href="{{ asset("profile.transactions") }}">Мои финансы</a>
                              </div>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </li>
                            @endauth
                      </ul>
                  </div>
              </div>
          </nav>
      </header>


      <div >
          @yield('content')
      </div>

      <br>
      <div class="clearfix"></div>
      <br>
      <footer>
          <div class="container">
              <nav class="text-center">
                  <ul class="navbar-nav footer-menu">
                      <li><a href="#">Главная  </a></li>|
                      <li><a href="#">Каталог</a></li>|
                      <li><a href="#">Автогарант</a></li>|
                      <li><a href="#">Обменник</a></li>|
                      <li><a href="#">Блог</a></li>|
                      @guest
                      <li><a href="{{ route('index.register.booster') }}">Бустерам</a></li>|
                      @endguest
                      <li><a href="#">О проекте</a></li>
                  </ul>
              </nav>
              <p class="text-center">Покупателю: Забудьте про риски нарваться на продавца - кидалу и про гарантов. Overlvl даёт <br> финансовую гарантию на любой товар или услугу.</p>
          </div>
      </footer>
  </div>
  </div>
        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="/website/js/jquery-3.3.1.min.js"></script>
        <script src="{{asset('js/app.js')}}"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="/website/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="/website/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="/website/js/main.js"></script>

        {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
  {{--<script src="{{ asset('website/js/jquery-slider.js') }}"></script>--}}
        <!-- <script type="text/javascript" src="/front/js/mdb.min.js"></script> -->

        <script>
            // popovers Initialization
            $(function () {
                $('[data-toggle="popover"]').popover()
            });

        </script>

        @stack('swipper')
        @stack('swippers')
    </body>
</html>
