$( function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "MMR " + ui.values[ 0 ] + " - MMR " + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "MMR " + $( "#slider-range" ).slider( "values", 0 ) + " - MMR " + $( "#slider-range" ).slider( "values", 1 ) );

    $( "#slider-range_1" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount_1" ).val( "MMR " + ui.values[ 0 ] + " - MMR " + ui.values[ 1 ] );
        }
    });
    $( "#amount_1" ).val( "MMR " + $( "#slider-range_1" ).slider( "values", 0 ) + " - MMR " + $( "#slider-range_1" ).slider( "values", 1 ) );

    $( "#slider-range_2" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount_2" ).val( "MMR " + ui.values[ 0 ] + " - MMR " + ui.values[ 1 ] );
        }
    });
    $( "#amount_2" ).val( "MMR " + $( "#slider-range_2" ).slider( "values", 0 ) + " - MMR " + $( "#slider-range_2" ).slider( "values", 1 ) );


    $( "#slider-range_3" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount_3" ).val( "MMR " + ui.values[ 0 ] + " - MMR " + ui.values[ 1 ] );
        }
    });
    $( "#amount_3" ).val( "MMR " + $( "#slider-range_3" ).slider( "values", 0 ) + " - MMR " + $( "#slider-range_3" ).slider( "values", 1 ) );


    $( "#slider-range_4" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount_4" ).val( "MMR " + ui.values[ 0 ] + " - MMR " + ui.values[ 1 ] );
        }
    });
    $( "#amount_4" ).val( "MMR " + $( "#slider-range_4" ).slider( "values", 0 ) + " - MMR " + $( "#slider-range_4" ).slider( "values", 1 ) );


} );
