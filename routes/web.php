<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/vue', function(){
  return view('layouts.index');
});


Auth::routes(['verify' => true]);


Route::get('/', 'WebsiteController@index')->name('index');
Route::get('/account/{type}', 'WebsiteController@index')->name('index.account.type');
Route::get('/register-booster', 'WebsiteController@registerB')->name('index.register.booster');
Route::get('/after-buy', 'WebsiteController@afterBuy')->name('index.after.buy');
Route::get('/methods', 'WebsiteController@methods')->name('index.methods');
Route::get('/choose/{type}/{id}', 'WebsiteController@choose')->name('index.choose');
Route::get('/catalog', 'WebsiteController@catalog')->name('index.catalog');
Route::get('/services', 'WebsiteController@services')->name('index.services');
Route::get('/booster/{id}', 'WebsiteController@booster')->name('index.booster');

Route::post('/languages{language}', 'LanguageController@index')->name('index.language');
Route::post('/choose/buy', 'WebsiteController@buy')->name("choose.buy");




Route::group(['prefix' => 'profile', "middleware"=>"verified"], function(){
   Route::get('/index', 'ProfileController@index')->name('profile.index');
   Route::get('/booster', 'ProfileController@booster')->name('profile.booster');
   Route::get('/deals', 'ProfileController@deals')->name('profile.deals');
   Route::get('/message/{id?}', 'ProfileController@message')->name('profile.message');
   Route::get('/methods', 'ProfileController@methods')->name('profile.methods');
   Route::get('/plans', 'ProfileController@plans')->name('profile.plans');
   Route::get('/transaction', 'ProfileController@transaction')->name('profile.transaction');
   Route::get('/work/{id?}', 'ProfileController@work')->name('profile.work');

   Route::post('/profile/index/update', "Profile\ProfileController@update")->name("profile.index.update");
});




Route::group(['prefix'=>'admin'], function(){
    // Auth
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');


   Route::group(['namespace'=>'Admin', "middleware"=>"auth:admin"], function(){
       // Conclusion
       Route::get('/conclusion', 'ConclusionController@index')->name('admin.conclusion.index');
       // Accounts
       Route::get('/accounts', 'AccountController@select')->name('admin.accounts.select');
       Route::get('/accounts/type/{type}', 'AccountController@index')->name('admin.accounts.index');
       Route::get('/accounts/type/{type}/create', 'AccountController@create')->name('admin.accounts.create');
       Route::get('/accounts/type/{type}/edit/{id}', 'AccountController@edit')->name('admin.accounts.edit');
       // Deals
       Route::get('/deals', 'DealController@index')->name('admin.deals.index');
       // Faqs
       Route::get('/faqs', 'FaqController@index')->name('admin.faqs.index');
       Route::get('/faqs/create', 'FaqController@create')->name('admin.faqs.create');
       Route::get('/faqs/{id}/edit', 'FaqController@edit')->name('admin.faqs.edit');
       // Reviews
       Route::get('/faqs', 'FaqController@index')->name('admin.faqs.index');
       Route::get('/faqs/create', 'FaqController@create')->name('admin.faqs.create');
       Route::get('/faqs/{id}/edit', 'FaqController@edit')->name('admin.faqs.edit');
       // Rules
       Route::get('/rules', 'RuleController@index')->name('admin.rules.index');
       Route::get('/rules/create', 'RuleController@create')->name('admin.rules.create');
       Route::get('/rules/{id}/edit', 'RuleController@edit')->name('admin.rules.edit');
       // Languages
       Route::get('/languages', 'LanguageController@select')->name('admin.languages.select');
       Route::get('/languages/{lang}', 'LanguageController@index')->name('admin.languages.index');
       Route::get('/languages/{lang}/create', 'LanguageController@create')->name('admin.languages.create');
       Route::get('/languages/{lang}/edit/{key}', 'LanguageController@edit')->name('admin.languages.edit');
       // Settings
       Route::group(["middleware"=>"admin"], function(){
           Route::get('/settings', 'SettingController@edit')->name('admin.settings.edit');
           Route::get('/settings/translate', 'SettingController@select')->name('admin.settings.translate');
       });

       // Users
       Route::get('/users', 'UserController@index')->name('admin.users.index');
       Route::get('/users/create', 'UserController@create')->name('admin.users.create');
       Route::get('/users/{id}/edit', 'UserController@edit')->name('admin.users.edit');
       // Admins
       Route::get('/admins', 'AdminController@index')->name('admin.admins.index');
       Route::get('/admins/create', 'AdminController@create')->name('admin.admins.create');
       Route::get('/admins/{id}/edit', 'AdminController@edit')->name('admin.admins.edit');

       // Reviews
       Route::get('/reviews', 'ReviewController@index')->name('admin.reviews.index');
       Route::get('/reviews/create', 'ReviewController@create')->name('admin.reviews.create');
       Route::get('/reviews/{id}/edit', 'ReviewController@edit')->name('admin.reviews.edit');

   });
});
