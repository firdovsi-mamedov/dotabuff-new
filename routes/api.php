<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
    WEBSITE API
 */

Route::get('/accounts/account/{type}', 'Website\Api\AccountController@index')->name('website-api.accounts.type');
Route::get('/accounts/wishlist', 'Website\Api\WishlistController@index')->name('website-api.accounts.wishlist.index');
Route::post('/accounts/wishlist', 'Website\Api\WishlistController@store')->name('website-api.accounts.wishlist.add');
Route::delete('/accounts/wishlist', 'Website\Api\WishlistController@destroy')->name('website-api.accounts.wishlist.destroy');

Route::get('/website/wishlist/get', 'Website\Api\WishlistController@get')->name('website-api.wishlist.get');


Route::get('/website/boosters', 'Website\Api\WebsiteController@boosters')->name('website-api.website.boosters');
Route::get('/website/filter', 'Website\Api\WebsiteController@filter')->name('website-api.filter');

Route::get('/website/messages/get', 'Website\Api\MessageController@index')->name('website-api.messages.get');
Route::get('/website/receiver/get', 'Website\Api\UserController@index')->name('website-api.receiver.get');
Route::post('/website/messages/post', 'Website\Api\MessageController@store')->name('website-api.message.post');
Route::get('/website/messages/conversations/get', 'Website\Api\ConversationController@index')->name('website-api.conversations.get');

Route::get('/website/work/get', 'Website\Api\WorkController@index')->name('website-api.work.messages.get');
Route::get('/website/work/receiver/get', 'Website\Api\UserController@indexWork')->name('website-api.work.receiver.get');
Route::get('/website/work/conversations/get', 'Website\Api\WorkConversationController@index')->name('website-api.work.conversations.get');
Route::post('/website/work/post', 'Website\Api\WorkController@store')->name('website-api.work.message.post');
Route::post('/website/work/accept', 'Website\Api\WorkController@accept')->name("website-api.work.accept");
Route::post('/website/work/closed', 'Website\Api\WorkController@closed')->name("website-api.work.closed");
Route::post('/website/work/done', 'Website\Api\WorkController@done')->name("website-api.work.done");
Route::post('/website/work/arbitrach', 'Website\Api\WorkController@arbitrach')->name("website-api.work.arbitrach");

Route::delete('/accounts/wishlist', 'Website\Api\WishlistController@destroy')->name('website-api.accounts.wishlist.destroy');

/**
    PROFILE API
 */

Route::get('/profile/services', 'Profile\Api\ProfileController@index')->name('profile-api.profile.services');
Route::post('/profile/services/status', 'Profile\Api\ProfileController@status')->name('profile-api.profile.services.status');
Route::post('/profile/services/changes', 'Profile\Api\ProfileController@changes')->name('profile-api.profile.services.changes');



Route::get('/profile/portfolio/get', 'Profile\Api\PortfolioController@get')->name('profile-api.portfolio.get');
Route::post('/profile/portfolio/upload', 'Profile\Api\PortfolioController@upload')->name('profile-api.portfolio.upload');
Route::delete('/profile/portfolio/delete', 'Profile\Api\PortfolioController@destroy')->name('profile-api.portfolio.delete');



/**
    ADMIN Panel API
 */

Route::get("/users", "Admin\Api\UserController@index")->name("admin-api.users.index");
Route::post("/users", "Admin\Api\UserController@store")->name("admin-api.users.store");
Route::put("/users", "Admin\Api\UserController@update")->name("admin-api.users.update");
Route::delete("/users", "Admin\Api\UserController@destroy")->name("admin-api.users.destroy");


Route::get("/admins", "Admin\Api\AdminController@index")->name("admin-api.admins.index");
Route::post("/admins", "Admin\Api\AdminController@store")->name("admin-api.admins.store");
Route::put("/admins", "Admin\Api\AdminController@update")->name("admin-api.admins.update");
Route::delete("/admins", "Admin\Api\AdminController@destroy")->name("admin-api.admins.destroy");


Route::get("/accounts", "Admin\Api\AccountController@index")->name("admin-api.accounts.index");
Route::post("/accounts", "Admin\Api\AccountController@store")->name("admin-api.accounts.store");
Route::put("/accounts", "Admin\Api\AccountController@update")->name("admin-api.accounts.update");
Route::delete("/accounts", "Admin\Api\AccountController@destroy")->name("admin-api.accounts.destroy");


Route::get("/conclusion", "Admin\Api\ConclusionController@index")->name("admin-api.conclusion.index");
Route::post("/conclusion", "Admin\Api\ConclusionController@store")->name("admin-api.conclusion.store");
Route::put("/conclusion", "Admin\Api\ConclusionController@update")->name("admin-api.conclusion.update");
Route::delete("/conclusion", "Admin\Api\ConclusionController@destroy")->name("admin-api.conclusion.destroy");


Route::get("/deals", "Admin\Api\DealController@index")->name("admin-api.deals.index");
Route::post("/deals", "Admin\Api\DealController@store")->name("admin-api.deals.store");
Route::put("/deals", "Admin\Api\DealController@update")->name("admin-api.deals.update");
Route::delete("/deals", "Admin\Api\DealController@destroy")->name("admin-api.deals.destroy");


Route::get("/faqs", "Admin\Api\FaqController@index")->name("admin-api.faqs.index");
Route::post("/faqs", "Admin\Api\FaqController@store")->name("admin-api.faqs.store");
Route::put("/faqs", "Admin\Api\FaqController@update")->name("admin-api.faqs.update");
Route::delete("/faqs", "Admin\Api\FaqController@destroy")->name("admin-api.faqs.destroy");


Route::get("/reviews", "Admin\Api\ReviewController@index")->name("admin-api.reviews.index");
Route::post("/reviews", "Admin\Api\ReviewController@store")->name("admin-api.reviews.store");
Route::put("/reviews", "Admin\Api\ReviewController@update")->name("admin-api.reviews.update");
Route::delete("/reviews", "Admin\Api\ReviewController@destroy")->name("admin-api.reviews.destroy");


Route::get("/rules", "Admin\Api\RuleController@index")->name("admin-api.rules.index");
Route::post("/rules", "Admin\Api\RuleController@store")->name("admin-api.rules.store");
Route::put("/rules", "Admin\Api\RuleController@update")->name("admin-api.rules.update");
Route::delete("/rules", "Admin\Api\RuleController@destroy")->name("admin-api.rules.destroy");


Route::get("/languages", "Admin\Api\LanguageController@index")->name("admin-api.languages.index");
Route::get("/languages/edit", "Admin\Api\LanguageController@edit")->name("admin-api.languages.edit");
Route::post("/languages", "Admin\Api\LanguageController@store")->name("admin-api.languages.store");
Route::put("/languages", "Admin\Api\LanguageController@update")->name("admin-api.languages.update");
Route::delete("/languages", "Admin\Api\LanguageController@destroy")->name("admin-api.languages.destroy");


Route::get("/settings", "Admin\Api\SettingController@edit")->name("admin-api.settings.index");
Route::post("/settings", "Admin\Api\SettingController@store")->name("admin-api.settings.store");
Route::put("/settings", "Admin\Api\SettingController@update")->name("admin-api.settings.update");
Route::delete("/settings", "Admin\Api\SettingController@destroy")->name("admin-api.settings.destroy");


Route::get("/statistic", "Admin\Api\StatisticController@index")->name("admin-api.statistic.index");
Route::post("/statistic", "Admin\Api\StatisticController@store")->name("admin-api.statistic.store");
Route::put("/statistic", "Admin\Api\StatisticController@update")->name("admin-api.statistic.update");
Route::delete("/statistic", "Admin\Api\StatisticController@destroy")->name("admin-api.statistic.destroy");
