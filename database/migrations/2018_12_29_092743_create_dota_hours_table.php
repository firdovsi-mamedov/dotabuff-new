<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDotaHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dota_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->text("notes");
            $table->integer("price");
            $table->string("login");
            $table->string("password");
            $table->string("email");
            $table->string("e_password");
            $table->text("description");
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dota_hours');
    }
}
