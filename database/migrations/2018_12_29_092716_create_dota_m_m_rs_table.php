<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDotaMMRsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dota_m_m_rs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("solo");
            $table->integer("party");
            $table->integer("rank");
            $table->integer("reputation");
            $table->integer("matches");
            $table->integer("dotabuff");
            $table->text("stream");
            $table->text("notes");
            $table->integer("price");
            $table->text("images")->nullable();
            $table->string("login");
            $table->string("password");
            $table->string("email");
            $table->string("e_password");
            $table->text("description");
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dota_m_m_rs');
    }
}
