<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            //
            $table->integer("role")->default(1);
            $table->string("dollar")->nullable();
            $table->string("euro")->nullable();
            $table->string("wmr")->default(0);
            $table->string("qiwi")->default(0);
            $table->string("visa_rus")->default(0);
            $table->string("yandex")->default(0);
            $table->string("paypal")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            //
            $table->dropColumn("role");
            $table->dropColumn("dollar");
            $table->dropColumn("euro");
            $table->dropColumn("wmr");
            $table->dropColumn("qiwi");
            $table->dropColumn("visa_rus");
            $table->dropColumn("yandex");
            $table->dropColumn("paypal");
        });
    }
}
