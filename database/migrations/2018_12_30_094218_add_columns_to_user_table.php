<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string("nickname")->nullable();
            $table->text("image")->nullable();
            // 0 - offline, 1 - online
            $table->integer("user_status")->default(0);
            $table->string("phone")->nullable();

            $table->integer("email_status")->default(0);
            $table->integer("browser_status")->default(0);
            $table->integer("sms_status")->default(0);

            $table->string("wallet_qiwi")->nullable();
            $table->string("wallet_webmoney")->nullable();
            $table->string("wallet_visa")->nullable();
            $table->string("wallet_paypal")->nullable();

            $table->integer("user_top")->default(0);
            $table->integer("rate")->default(0);
            $table->integer("stage")->default(0);

            $table->integer("enable_notification")->default(0);

            $table->integer("package_status")->default(0);

            $table->text("solo_text_ru")->nullable();
            $table->text("solo_text_en")->nullable();
            $table->text("learning_text_ru")->nullable();
            $table->text("learning_text_en")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn("nickname");
            $table->dropColumn("image");
            $table->dropColumn("user_status");
            $table->dropColumn("phone");
            $table->dropColumn("email_status");
            $table->dropColumn("browser_status");
            $table->dropColumn("sms_status");
            $table->dropColumn("wallet_qiwi");
            $table->dropColumn("wallet_webmoney");
            $table->dropColumn("wallet_visa");
            $table->dropColumn("wallet_paypal");
            $table->dropColumn("enable_notification");
            $table->dropColumn("package_status");
            $table->dropColumn("solo_text_ru");
            $table->dropColumn("solo_text_en");
            $table->dropColumn("learning_text_ru");
            $table->dropColumn("learning_text_en");
        });
    }
}
