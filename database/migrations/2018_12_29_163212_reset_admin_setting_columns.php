<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetAdminSettingColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            //
            $table->dropColumn("role");
            $table->dropColumn("dollar");
            $table->dropColumn("euro");
            $table->dropColumn("wmr");
            $table->dropColumn("qiwi");
            $table->dropColumn("visa_rus");
            $table->dropColumn("yandex");
            $table->dropColumn("paypal");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            //
        });
    }
}
