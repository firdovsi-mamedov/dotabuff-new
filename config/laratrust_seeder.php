<?php

return [
    'role_structure' => [
        'booster' => [
            'main' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'user' => [
            'main' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
    ],
    'permission_structure' => [

    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
