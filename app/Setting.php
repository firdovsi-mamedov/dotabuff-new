<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['dollar', 'euro', 'wmr', 'qiwi', 'visa_rus', 'yandex', 'paypal'];
}
