<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = ["user_id", "wish_user", "account_id", "account_type"];

    public function account(){
        return $this->belongsTo("App\DotaMMR", "account_id");
    }
}
