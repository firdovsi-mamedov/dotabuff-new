<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkMessage extends Model
{
    //
    protected $fillable = ["sender_id", "receiver_id", "message", "status", "conversation_id", "order_status"];

    public function sender(){
        return $this->belongsTo("App\User", "sender_id");
    }
    public function receiver(){
        return $this->belongsTo("App\User", "receiver_id");
    }
}
