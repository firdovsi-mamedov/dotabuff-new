<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class OrderClosed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request;
    public $tries = 3;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->request = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $conversation = \App\WorkConversation::find($this->request["conversation_id"]);
        $conversation->status = 4;
        $conversation->save();

        $receiver = \App\User::find($this->request["receiver_id"]);
        $sender = \App\User::find($this->request["sender_id"]);

        Mail::to($receiver->email)->send(new \App\Mail\OrderDone($this->request));
        Mail::to($sender->email)->send(new \App\Mail\OrderDone($this->request));

    }
}
