<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class AccountBuy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $tries = 3;
    protected $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->request = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $account_id = $this->request["account_id"];
        $type = $this->request["type"];
        switch($type){
            case "mmr":
                $account = \App\DotaMMR::find($account_id);
                break;
            case "tbd":
                $account = \App\DotaTBD::find($account_id);
                break;
            case "hours":
                $account = \App\DotaHour::find($account_id);
            default:
                $account = \App\DotaMMR::find($account_id);
        }
        Mail::to($this->request['email'])->send(new \App\Mail\AccountBuy($account));

    }
}
