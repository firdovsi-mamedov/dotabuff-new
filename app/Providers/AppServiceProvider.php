<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        //
        Resource::withoutWrapping();

        View::composer("profile.layouts.master", function ($view) {
            //
            $view->with("messagesCount", \App\Message::where([["receiver_id", "=", Auth::id()], ["status", "=", 0]])->get()->groupBy("conversation_id")->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
