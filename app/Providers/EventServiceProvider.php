<?php

namespace App\Providers;

use App\Events\UserRegistered;
use App\Events\BoosterArbitrach;
use App\Events\UserArbitrach;
use App\Events\BoosterDone;
use App\Events\BoosterSendMessageChat;
use App\Events\BoosterSendMessageWork;
use App\Events\OrderClosed;
use App\Events\UserAccept;
use App\Events\UserSendMessageChat;
use App\Events\UserSendMessageWork;
use App\Events\AccountBuy;

use App\Listeners\AddServiceCalibres;
use App\Listeners\AddServiceLearnings;
use App\Listeners\AddServiceOtmits;
use App\Listeners\AddServiceSolos;
use App\Listeners\BoosterArbirtachListener;
use App\Listeners\UserArbitrachListener;
use App\Listeners\BoosterDoneListener;
use App\Listeners\BoosterSendMessageChatListener;
use App\Listeners\BoosterSendMessageWorkListener;
use App\Listeners\OrderClosedListener;
use App\Listeners\UserAcceptListener;
use App\Listeners\UserSendMessageChatListener;
use App\Listeners\UserSendMessageWorkListener;
use App\Listeners\AccountBuyListener;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserRegistered::class => [
            AddServiceSolos::class,
            AddServiceCalibres::class,
            AddServiceOtmits::class,
            AddServiceLearnings::class
        ],
        BoosterArbitrach::class => [
            BoosterArbirtachListener::class,
        ],
        UserArbitrach::class => [
            UserArbitrachListener::class,
        ],
        BoosterDone::class => [
            BoosterDoneListener::class,
        ],
        BoosterSendMessageChat::class => [
            BoosterSendMessageChatListener::class,
        ],
        BoosterSendMessageWork::class => [
            BoosterSendMessageWorkListener::class,
        ],
        OrderClosed::class => [
            OrderClosedListener::class,
        ],
        UserAccept::class => [
            UserAcceptListener::class,
        ],
        UserSendMessageChat::class => [
            UserSendMessageChatListener::class,
        ],
        UserSendMessageWork::class => [
            UserSendMessageWorkListener::class,
        ],
        AccountBuy::class => [
            AccountBuyListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
