<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    //
    protected $fillable = ["sender_id", "receiver_id"];

     //
     public function receiver(){
        return $this->hasOne("App\User", "id", "receiver_id");
    }

    //
    public function sender(){
        return $this->hasOne("App\User", "id", "sender_id");
    }
}
