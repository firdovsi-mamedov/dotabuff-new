<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable  = ['title_ru', 'title_en', 'content_ru', 'content_en'];
}
