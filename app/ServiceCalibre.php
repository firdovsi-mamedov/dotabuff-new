<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCalibre extends Model
{
    //
    protected $fillable = ["status", "cost_website", "cost_original", "days", "user_id"];
}
