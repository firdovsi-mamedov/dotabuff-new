<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    // Shows index page
    public function select(){
        return view("adminpanel.accounts.select");
    }

    // Shows index page
    public function index($type){
        //
        $type = json_encode(["type"=>$type]);
        //
        return view("adminpanel.accounts.index", compact("type"));
    }

    //
    public function create($type){
        $account = json_encode(['type'=>$type]);
        return view("adminpanel.accounts.create", compact("account", "type"));
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($type, $id){
        $account = json_encode(['type'=>$type]);
        return view("adminpanel.accounts.edit", compact("account", "type", "id"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.accounts.index");
    }
}
