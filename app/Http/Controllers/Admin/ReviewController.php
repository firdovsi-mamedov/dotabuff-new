<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.reviews.index");
    }

    //
    public function create(){
        return view("adminpanel.reviews.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        //
        $review = Review::find($id);
        //
        return view("adminpanel.reviews.edit", compact("review"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy(){
    }
}
