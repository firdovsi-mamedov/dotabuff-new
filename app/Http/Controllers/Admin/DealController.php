<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.deals.index");
    }

    //
    public function create(){
        return view("adminpanel.deals.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        return view("adminpanel.deals.edit", compact("admins"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.deals.index");
    }
}
