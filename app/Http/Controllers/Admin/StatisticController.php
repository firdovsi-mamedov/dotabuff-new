<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatisticController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.statistic.index");
    }

    //
    public function create(){
        return view("adminpanel.statistic.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        return view("adminpanel.statistic.edit", compact("admins"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.statistic.index");
    }
}
