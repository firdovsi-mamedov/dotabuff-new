<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.users.index");
    }

    //
    public function create(){
        return view("adminpanel.users.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        $user = User::find($id);
        return view("adminpanel.users.edit", compact("user"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        User::destroy($id);
        return redirect()->route("adminpanel.users.index");
    }
}
