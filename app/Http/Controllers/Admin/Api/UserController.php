<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request['id']);
        }
        //
        $per_page = $request['per_page'];
        //
        $user = User::paginate($per_page);
        //
        return UserResource::collection($user);
    }
    //
    public function store(UserRequest $request){
        User::create([
            "name"  =>  $request['name'],
            "email" =>  $request['email'],
            "password"  => bcrypt($request['password'])
        ]);
        return response()->json(['response'=>'ok'], 200);
    }
    //
    public function edit($id){
        $user = User::find($id);
        return new UserResource($user);
    }
    //
    public function update(Request $request){
        User::find($request['id'])->update([
            "name"  =>  $request['name'],
            "email" =>  $request['email'],
            "password"  => bcrypt($request['password'])
        ]);
        return response()->json(["response"=> "ok"], 200);
    }
    //
    public function destroy(Request $request){
        User::destroy($request['id']);
        return response()->json(["response"=> "ok"], 200);
    }
}
