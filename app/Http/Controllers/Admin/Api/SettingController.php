<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Resources\SettingResource;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function edit(){
        return new SettingResource(Setting::find(1));
    }

    //
    public function update(Request $request){
        Setting::find(1)->update($request->all());
    }
}
