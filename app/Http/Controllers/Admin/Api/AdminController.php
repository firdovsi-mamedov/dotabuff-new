<?php

namespace App\Http\Controllers\Admin\Api;

use App\Admin;
use App\Http\Requests\AdminRequest;
use App\Http\Resources\AdminResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request['id']);
        }
        //
        $per_page = $request['per_page'];
        //
        $user = Admin::paginate($per_page)->except(1);
        //
        return AdminResource::collection($user);
    }
    //
    public function store(AdminRequest $request){
        Admin::create($request->all());
        return response()->json(['response'=>'ok'], 200);
    }
    //
    public function edit($id){
        $user = Admin::find($id);
        return new AdminResource($user);
    }
    //
    public function update(Request $request){
        Admin::find($request['id'])->update($request->all());
        return response()->json(["response"=> "ok"], 200);
    }
    //
    public function destroy(Request $request){
        Admin::destroy($request['id']);
        return response()->json(["response"=> "ok"], 200);
    }
}
