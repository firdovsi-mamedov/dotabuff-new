<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class LanguageController extends Controller
{
    //
    public function index(Request $request){
        $filePath = base_path() . "/resources/lang/{$request['lang']}.json";
        //
        $langs    = json_decode(File::get($filePath), true);
        //
        return response()->json($langs, 200);
    }

    //
    public function store(Request $request){
        //
        $filePath                       = base_path() . "/resources/lang/{$request['lang']}.json";
        $lang                           = json_decode(File::get($filePath), true);
        $lang[$request['key']] = $request['value'];
        File::replace($filePath, json_encode($lang, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return response()->json(['success' => $request['key']], 201);


    }

    //
    public function edit(Request $request){
        //
        $filePath = base_path() . "/resources/lang/{$request['lang']}.json";
        //
        $lang     = json_decode(File::get($filePath), true);
        //
        return response()->json(['value' => $lang[$request['key']], 'key'=>$request['key']], 200, [], JSON_UNESCAPED_UNICODE);

    }

    //
    public function update(Request $request)
    {
        $filePath                       = base_path() . "/resources/lang/{$request['lang']}.json";
        $lang                           = json_decode(File::get($filePath), true);
        $lang[$request['key']] = $request['value'];
        File::replace($filePath, json_encode($lang, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return response()->json(['success' => 'ok'])->setStatusCode(200);
    }

}
