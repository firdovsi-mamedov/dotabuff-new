<?php

namespace App\Http\Controllers\Admin\Api;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;
use App\Http\Resources\ReviewResource;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    //
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request['id']);
        }
        //
        $per_page = $request['per_page'];
        //
        $faq = Review::paginate($per_page);
        //
        return ReviewResource::collection($faq);
    }
    //
    public function store(ReviewRequest $request){
        Review::create($request->all());
        return response()->json(['response'=>'ok'], 200);
    }
    //
    public function edit($id){
        $faq = Review::find($id);
        return new ReviewResource($faq);
    }
    //
    public function update(Request $request){
        Review::find($request['id'])->update($request->all());
        return response()->json(["response"=> "ok"], 200);
    }
    //
    public function destroy(Request $request){
        Review::destroy($request['id']);
        return response()->json(["response"=> "ok"], 200);
    }
}
