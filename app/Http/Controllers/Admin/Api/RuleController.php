<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Requests\RuleRequest;
use App\Http\Resources\RuleResource;
use App\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuleController extends Controller
{
    //
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request['id']);
        }
        //
        $per_page = $request['per_page'];
        //
        $rule = Rule::paginate($per_page);
        //
        return RuleResource::collection($rule);
    }
    //
    public function store(RuleRequest $request){
        Rule::create($request->all());
        return response()->json(['response'=>'ok'], 200);
    }
    //
    public function edit($id){
        $rule = Rule::find($id);
        return new RuleResource($rule);
    }
    //
    public function update(Request $request){
        Rule::find($request['id'])->update($request->all());
        return response()->json(["response"=> "ok"], 200);
    }
    //
    public function destroy(Request $request){
        Rule::destroy($request['id']);
        return response()->json(["response"=> "ok"], 200);
    }
}
