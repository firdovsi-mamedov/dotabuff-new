<?php

namespace App\Http\Controllers\Admin\Api;

use App\Faq;
use App\Http\Requests\FaqRequest;
use App\Http\Resources\FaqResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    //
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request['id']);
        }
        //
        $per_page = $request['per_page'];
        //
        $faq = Faq::paginate($per_page);
        //
        return FaqResource::collection($faq);
    }
    //
    public function store(FaqRequest $request){
        Faq::create($request->all());
        return response()->json(['response'=>'ok'], 200);
    }
    //
    public function edit($id){
        $faq = Faq::find($id);
        return new FaqResource($faq);
    }
    //
    public function update(Request $request){
        Faq::find($request['id'])->update($request->all());
        return response()->json(["response"=> "ok"], 200);
    }
    //
    public function destroy(Request $request){
        Faq::destroy($request['id']);
        return response()->json(["response"=> "ok"], 200);
    }
}
