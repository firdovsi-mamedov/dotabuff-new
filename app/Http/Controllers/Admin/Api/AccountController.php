<?php

namespace App\Http\Controllers\Admin\Api;

use App\DotaHour;
use App\DotaMMR;
use App\DotaTBD;
use App\Http\Requests\AccountRequest;
use App\Http\Resources\AccountResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function index(Request $request){
        //
        if($request['id']){
            return $this->edit($request->all());
        }
        //
        $account_type = $request['type'];

        switch ($account_type){
            case 1:
                return $this->dota_mmr();
                break;
            case 2:
                return $this->dota_tbd();
                break;
            case 3:
                return $this->dota_hours();
                break;
            default:
                return $this->dota_mmr();
        }

    }
    //
    public function dota_mmr($id = null){
        //
        //
        if($id !== null){
            $item = DotaMMR::find($id);
            return response()->json($item, 200, [], JSON_UNESCAPED_UNICODE);
        }

        $items = DotaMMR::all();

        return AccountResource::collection($items);
    }
    //
    public function dota_tbd($id = null){
        //
        if($id !== null){
            $item = DotaTBD::find($id);
            return response()->json($item, 200, [], JSON_UNESCAPED_UNICODE);
        }

        $items = DotaTBD::all();

        return AccountResource::collection($items);
    }
    //
    public function dota_hours($id = null){
        //
        if($id !== null){
            $item = DotaHour::find($id);
            return response()->json($item, 200, [], JSON_UNESCAPED_UNICODE);
        }

        $items = DotaHour::all();

        return AccountResource::collection($items);
    }


    public function store(AccountRequest $request){
        //
        $account_type = $request['type'];

        switch ($account_type){
            case 1:
                return $this->dota_mmr_store($request->all());
                break;
            case 2:
                return $this->dota_tbd_store($request->all());
                break;
            case 3:
                return $this->dota_hours_store($request->all());
                break;
            default:
                return $this->dota_mmr_store($request->all());
        }
    }

    //
    public function dota_mmr_store($request){
        //
        $items = DotaMMR::create($request);

        return new AccountResource($items);
    }
    //
    public function dota_tbd_store($request){
        //
        $items = DotaTBD::create($request);

        return new AccountResource($items);
    }
    //
    public function dota_hours_store($request){
        //
        $items = DotaHour::create($request);

        return request()->json($items, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function edit($request){
        $account_type = $request['type'];

        switch ($account_type){
            case 1:
                return $this->dota_mmr($request['id']);
                break;
            case 2:
                return $this->dota_tbd($request['id']);
                break;
            case 3:
                return $this->dota_hours($request['id']);
                break;
            default:
                return $this->dota_mmr($request['id']);
        }
    }

    //
    public function update(AccountRequest $request){
        //
        $account_type = $request['type'];

        switch ($account_type){
            case 1:
                return $this->dota_mmr_update($request->all());
                break;
            case 2:
                return $this->dota_tbd_update($request->all());
                break;
            case 3:
                return $this->dota_hours_update($request->all());
                break;
            default:
                return $this->dota_mmr_update($request->all());
        }
    }


    //
    public function dota_mmr_update($request){
        //
        $items = DotaMMR::find($request['account_id'])->update($request);

        return response()->json($items, 200, [], JSON_UNESCAPED_UNICODE);
    }
    //
    public function dota_tbd_update($request){
        //
        $items = DotaTBD::find($request['account_id'])->update($request);

        return response()->json($items, 200, [], JSON_UNESCAPED_UNICODE);
    }
    //
    public function dota_hours_update($request){
        //
        $items = DotaHour::find($request['account_id'])->update($request);

        return response()->json($items, 200, [], JSON_UNESCAPED_UNICODE);
    }


    public function destroy(Request $request){

        $account_type = $request['type'];

        switch ($account_type){
            case 1:
                DotaMMR::destroy($request['account_id']);
                break;
            case 2:
                DotaTBD::destroy($request['account_id']);
                break;
            case 3:
                DotaHour::destroy($request['account_id']);
                break;
            default:
                DotaMMR::destroy($request['account_id']);
        }
    }
}
