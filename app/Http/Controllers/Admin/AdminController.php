<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.admins.index");
    }

    //
    public function create(){
        return view("adminpanel.admins.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        $admin = Admin::find($id);
        return view("adminpanel.admins.edit", compact("admin"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.admins.index");
    }
}
