<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.faqs.index");
    }

    //
    public function create(){
        return view("adminpanel.faqs.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        //
        $faq = Faq::find($id);
        //
        return view("adminpanel.faqs.edit", compact("faq"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy(){
    }
}
