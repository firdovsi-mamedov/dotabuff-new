<?php

namespace App\Http\Controllers\Admin;

use App\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuleController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.rules.index");
    }

    //
    public function create(){
        return view("adminpanel.rules.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        //
        $rule = Rule::find($id);
        //
        return view("adminpanel.rules.edit", compact("rule"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy(){
    }
}
