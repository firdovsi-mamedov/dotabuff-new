<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    // Shows index page
    public function select(){
        return view("adminpanel.languages.select");
    }
    //
    public function index($lang_slug){
        $lang = json_encode(['key'=>$lang_slug]);
        return view("adminpanel.languages.index", compact('lang'));
    }

    //
    public function create($lang){
        $lang = json_encode(['lang'=>$lang]);
        return view("adminpanel.languages.create", compact("lang"));
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($lang, $key){
        $lang_key = json_encode(["key"=>$key, 'lang'=>$lang]);
        return view("adminpanel.languages.edit", compact("lang_key"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.languages.index");
    }
}
