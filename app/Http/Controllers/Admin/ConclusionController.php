<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConclusionController extends Controller
{
    // Shows index page
    public function index(){
        return view("adminpanel.conclusion.index");
    }

    //
    public function create(){
        return view("adminpanel.conclusion.create");
    }

    //
    public function store(Request $request){

    }

    //
    public function edit($id){
        return view("adminpanel.conclusion.edit", compact("admins"));
    }

    //
    public function upload(Request $request, $id){

    }

    //
    public function destroy($id){
        Admin::destroy($id);
        return redirect()->route("adminpanel.conclusion.index");
    }
}
