<?php

namespace App\Http\Controllers\Profile\Api;

use App\ServiceCalibre;
use App\ServiceLearning;
use App\ServiceOtmit;
use App\ServiceSolo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(Request $request){
        $service_type  = $request['service_type'];
        $user_id       = $request['user_id'];
        switch ($service_type){
            case 1:
                $services = ServiceSolo::where("user_id", $user_id)->get();
                break;
            case 2:
                $services = ServiceCalibre::where("user_id", $user_id)->get();
                break;
            case 3:
                $services = ServiceOtmit::where("user_id", $user_id)->get();
                break;
            case 4:
                $services = ServiceLearning::where("user_id", $user_id)->get();
                break;
            default:
        }
        return response($services);
    }
    public function status(Request $request){
        $service_type = $request['service_type'];
        $service_id   = $request['service_id'];
        $status       = $request['status'];

        switch($service_type){
            case 1:
                ServiceSolo::find($service_id)->update(["status"=> $status]);
                break;
            case 2:
                ServiceCalibre::find($service_id)->update(["status"=> $status]);
                break;
            case 3:
                ServiceOtmit::find($service_id)->update(["status"=> $status]);
                break;
            case 4:
                ServiceLearning::find($service_id)->update(["status"=> $status]);
                break;

            default:
        }

        return response()->json(['response'=>'ok'], 200);
    }

    public function changes(Request $request){
        $service_type = $request['service_type'];
        $service_id   = $request['service_id'];
        $column_name  = $request['column_name'];
        $value        = $request['value'];
        switch ($service_type){
            case 1:
                ServiceSolo::find($service_id)->update([$column_name => $value]);
                break;
            case 2:
                ServiceCalibre::find($service_id)->update([$column_name => $value]);
                break;
            case 3:
                ServiceOtmit::find($service_id)->update([$column_name => $value]);
                break;
            case 4:
                ServiceLearning::find($service_id)->update([$column_name => $value]);
                break;
            default:

        }
        return response()->json(['response'=> 'ok'], 200);
    }
}
