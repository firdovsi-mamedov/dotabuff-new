<?php

namespace App\Http\Controllers\Profile\Api;

use App\Portfolio;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PortfolioController extends Controller
{
    //
    public function upload(Request $request){
        $user_id = $request["user_id"];
        // Берём объект файл
        $path = $request->file('file');
        // Мы берем расширение файла (jpg, png, gif)
        $extension = $path->getClientOriginalExtension();
        // Создаем название для файла (1.jpg, 2.jpg)
        $name = uniqid("portfolio_", true).".{$extension}";
        // Сохраняем файл на диске (FTP, Local)
        Storage::disk('portfolio')->putFileAs("/", new File($path), $name);
        // Возвращаем название файла чтобы выводит на FrontEnd
        Portfolio::create([
            "image"=>$name,
            "user_id"=>$user_id,
        ]);

        return $name;
    }

    public function get(Request $request){
        $user_id = $request["user_id"];
        $portfolio = Portfolio::where("user_id", $user_id)->get();
        return response()->json($portfolio, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function destroy(Request $request){
        $user_id = $request["user_id"];
        // Берем название файла
        $name = $request['image'];
        // Создаем путь до файла
        $path = public_path("uploads/portfolio/{$name}");
        // Удаляем файл с директории
        Portfolio::where("user_id", $user_id)->where("image", $name)->delete();
        if(!unlink($path)){
            return 403;
        }
        return response()->json(['response'=>'ok'], 200, [], JSON_UNESCAPED_UNICODE);
    }


}
