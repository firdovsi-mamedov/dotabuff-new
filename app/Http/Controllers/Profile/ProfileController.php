<?php

namespace App\Http\Controllers\Profile;

use App\User;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    //

    public function update(Request $request){

        $user = User::find(Auth::id());

        if($request->hasFile("image")){
            $image = $request->file("image");
            $extenstion = $image->getClientOriginalExtension();
            $fullname = uniqid("image_", false).".{$extenstion}";
            Storage::disk("avatars")->putFileAs( "/", new File($image), $fullname);
        }else{
            $fullname = $user->image;
        }


        $user->nickname = $request->nickname;
        $user->phone    = $request->phone;
        $user->user_status = $request->user_status;
        $user->image = $fullname;

        $user->wallet_qiwi    = $request->wallet_qiwi;
        $user->wallet_webmoney    = $request->wallet_webmoney;
        $user->wallet_visa    = $request->wallet_visa;
        $user->wallet_paypal    = $request->wallet_paypal;

        $user->email_status = $request->email_status ? 1 : 0;
        $user->sms_status = $request->sms_status ? 1 : 0;
        $user->browser_status = $request->browser_status ? 1 : 0;

        $user->save();

        return redirect()->back();
    }
}
