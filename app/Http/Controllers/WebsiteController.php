<?php

namespace App\Http\Controllers;


use App\Faq;
use App\Portfolio;
use App\Review;
use App\Role;
use App\Rule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class WebsiteController extends Controller
{

    const ACCOUNT_SOLD = 1;
    const ACCOUNT_HAVE = 0;

    public function index($type = null){

        if($type !== null){
            switch ($type){
                case "mmr":
                    $type = 1;
                    break;
                case "tbd":
                    $type = 2;
                    break;
                case "hours":
                    $type = 3;
                    break;
                default:
                    $type = 1;
            }
        }else{
            $type = 1;
        }

        // Accounts Count
        $accountCount =

        $reviews = Review::paginate(9);
        $faqs = Faq::all();
        $lang = App::getLocale();

        $account = json_encode(["type"=>$type]);

        $counts = $this->getCounts();
        return view('website.index', compact("type", "account", "reviews", "faqs", "lang", "counts"));
    }

    //
    public function registerB(){
        $rules = Rule::all();
        return view('website.register-booster', compact("rules"));
    }

    //
    public function afterBuy(){
        return view('website.after-buy');
    }

    //
    public function methods(){
        return view('website.methods');
    }

    //
    public function choose($type, $id){
        switch($type){
            case "mmr":
                $account = \App\DotaMMR::find($id);
                break;
            case "tbd":
                $account = \App\DotaTBD::find($id);
                break;
            case "hours":
                $account = \App\DotaHour::find($id);
            default:
            $account = \App\DotaMMR::find($id);
        }
        return view('website.choose', compact('account', "id", "type"));
    }

    //
    public function catalog(){
        $faqs = Faq::all();
        $lang = App::getLocale();
        return view('website.index', compact("faqs", "lang"));
    }

    //
    public function services(){
        $boosters = Role::with('users')->where('name', 'admin')->count();
        $faqs = Faq::all();
        $lang = App::getLocale();

        return view('website.catalog', compact("boosters", "faqs", "lang"));
    }

    //
    public function booster($id){
        $booster = User::find($id);
        $user = Auth::user();
        $portfolio = Portfolio::where("user_id", $id)->get();
        return view('website.booster', compact("booster", "id", "user", "portfolio"));
    }


    protected function getCounts(){
        $mmr = \App\DotaMMR::all()->count();
        $tbd = \App\DotaTBD::all()->count();
        $hours = \App\DotaHour::all()->count();
        $all = $mmr + $tbd + $hours;

        $currentMonth = date('m');
        $mmr = \App\DotaMMR::where("status", 1)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
        $tbd = \App\DotaTBD::where("status", 1)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
        $hours = \App\DotaHour::where("status", 1)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
        $soldMonth = $mmr + $tbd + $hours;

        $mmr = \App\DotaMMR::where("status", 1)->count();
        $tbd = \App\DotaTBD::where("status", 1)->count();
        $hours = \App\DotaHour::where("status", 1)->count();
        $soldAll = $mmr + $tbd + $hours;

        $arr = ["all"=>$all, "soldMonth"=>$soldMonth, "soldAll"=>$soldAll];
        return $arr;
    }

    public function buy(Request $request){
        $account = $this->getAccount($request);

        if(filter_var($request["email"], FILTER_VALIDATE_EMAIL) && $request["payment"] !== 0){
            event(new \App\Events\AccountBuy($request->all()));
            return redirect(route("index.after.buy"))->with("account", "Status");
        }else{
            return redirect()->back();
        }
    }

    public function getAccount($request){
        $account_id = $request["account_id"];
        $type = $request["type"];
        switch($type){
            case "mmr":
                $account = \App\DotaMMR::find($account_id);
                break;
            case "tbd":
                $account = \App\DotaTBD::find($account_id);
                break;
            case "hours":
                $account = \App\DotaHour::find($account_id);
            default:
                $account = \App\DotaMMR::find($account_id);
        }
        return $account;
    }
}
