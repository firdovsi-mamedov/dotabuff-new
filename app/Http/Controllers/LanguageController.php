<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{

    public function index(Request $request)
    {
        if($request['language']==="en"){
            //
            Session::put('locale', 'en');
            //
        }else if($request["language"] === "ru"){
            //
            Session::put('locale', 'ru');
            //
        }else{
            Session::put('locale', 'en');
        }
        return redirect()->route('index');
    }


}
