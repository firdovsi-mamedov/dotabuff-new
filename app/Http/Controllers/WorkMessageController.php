<?php

namespace App\Http\Controllers;

use App\WorkMessage;
use Illuminate\Http\Request;

class WorkMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkMessage  $workMessage
     * @return \Illuminate\Http\Response
     */
    public function show(WorkMessage $workMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkMessage  $workMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkMessage $workMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkMessage  $workMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkMessage $workMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkMessage  $workMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkMessage $workMessage)
    {
        //
    }
}
