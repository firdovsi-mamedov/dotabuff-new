<?php

namespace App\Http\Controllers;

use App\WorkConversation;
use Illuminate\Http\Request;

class WorkConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkConversation  $workConversation
     * @return \Illuminate\Http\Response
     */
    public function show(WorkConversation $workConversation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkConversation  $workConversation
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkConversation $workConversation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkConversation  $workConversation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkConversation $workConversation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkConversation  $workConversation
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkConversation $workConversation)
    {
        //
    }
}
