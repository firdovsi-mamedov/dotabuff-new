<?php

namespace App\Http\Controllers\Website\Api;

use App\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    //
    public function index(Request $request){
        //
        $account_type = $request["account_type"];
        $user_id = $request["user_id"];
        //
        $wishlist = Wishlist::where("user_id", $user_id)->where("account_type", $account_type)->get();
        //
        return response()->json($wishlist, 200);

    }

    //
    public function store(Request $request){
        $account_id = $request['account_id'];
        $user_id    = $request['user_id'];
        $account_type  = $request['account_type'];
        Wishlist::create([
            "user_id"     =>  $user_id,
            "account_id"  =>  $account_id,
            "account_type"  =>  $account_type
        ]);
        return response()->json(["response"=> "ok"], 200);
    }

    //
    public function destroy(Request $request){
        $account_id = $request['account_id'];
        $user_id    = $request['user_id'];
        $account_type    = $request['account_type'];
        Wishlist::where(["account_id"=>$account_id, "user_id"=>$user_id, "account_type"=>$account_type])->delete();
        return response()->json(["response"=> "ok"], 200);
    }


    public function get(Request $request){
        $user_id = $request["user_id"];
        $wishlist = Wishlist::where("user_id", $user_id)->with("account")->get();

        return response()->json($wishlist, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
