<?php

namespace App\Http\Controllers\Website\Api;

use App\WorkConversation;
use App\WorkMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkController extends Controller
{
    public function index(Request $request){
        //
        if($request["conversation_id"] !== null){
            $messages = $this->messages( $request["conversation_id"]);
            return response()->json($messages);
        }
        return response()->json($receiver_id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function messages($conversation_id)
    {
        //
        $messages = \App\WorkMessage::where("conversation_id", $conversation_id)->with("receiver", "sender")->get();
        return $messages;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
        $messages = \App\WorkMessage::create($request->all());
        $user = \App\User::find($request["sender_id"]);

        if($user->role === "booster"){
            event(new \App\Events\BoosterSendMessageWork($request->all()));
        }else{
            event(new \App\Events\UserSendMessageWork($request->all()));
        }

        $message = \App\WorkMessage::where("id", $messages->id)->with("receiver", "sender")->first();
        return response()->json($message, 200);
    }

    public function createConversation($sender_id, $receiver_id){
        $conversation = WorkConversation::create(["sender_id"=>$sender_id, "receiver_id"=>$receiver_id]);
        return $conversation;
    }


    public function done(Request $request){
        $user = \App\User::find($request["sender_id"]);

        $message = new \App\WorkMessage();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Исполнитель {$user->name} сообщил о выполнении заказа. Заказчик, проверьте выполнение в течение 24 часов и подтвержите это.";
        $message->save();

        event(new \App\Events\BoosterDone($request->all()));

        $data = \App\WorkMessage::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }

    public function accept(Request $request){

        $user = \App\User::find($request["sender_id"]);

        $message = new \App\WorkMessage();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Заказчик {$user->name} принял заказ. Благодарим за пользование сервисом.";
        $message->save();

        $data = \App\WorkMessage::where("id", $message->id)->with("receiver", "sender")->first();

        event(new \App\Events\UserAccept($request->all()));

        return response()->json($data, 200);
    }


    public function arbitrach(Request $request){

        $user = \App\User::find($request["sender_id"]);

        $message = new \App\WorkMessage();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;

        if($user->role == "booster"){
            $message->message = "Заказчик {$user->name} открыл спор. Пожалуйста, как можно скорее решите проблему.";

            event(new \App\Events\BoosterArbitrach($request->all()));
        }else{
            $message->message = "Исполнитель {$user->name} открыл спор. Пожалуйста, как можно скорее решите проблему.";

            event(new \App\Events\UserArbitrach($request->all()));
        }
        $message->save();

        $data = \App\WorkMessage::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }

    public function closed(Request $request){

        $message = new \App\WorkMessage();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Заказ закрыт администрацией.";
        $message->save();

        event(new \App\Events\OrderClosed($request->all()));

        $data = \App\WorkMessage::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }
}
