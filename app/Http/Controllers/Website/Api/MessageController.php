<?php

namespace App\Http\Controllers\Website\Api;

use App\Conversation;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\PaymentDone;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    public function index(Request $request){
        //
        if($request["conversation_id"] !== null){
            $messages = $this->messages( $request["conversation_id"]);
            return response()->json($messages, 200);
        }
        if($request->has("receiver_id")){
            $conversation = $this->getConversation($request["sender_id"], $request["receiver_id"]);
            $messages = $this->messages($conversation->id);
            return response()->json($messages, 200);
        }
        return response()->json($receiver_id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function messages($conversation_id)
    {
        //
        $messages = \App\Message::where("conversation_id", $conversation_id)->with("receiver", "sender")->get();
        return $messages;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
        if($request->has("conversation_id")){
            $messages = \App\Message::create($request->all());
        }else{
            $conversation = $this->getConversation($request["sender_id"], $request["receiver_id"]);
            $request["conversation_id"] = $conversation->id;
            $messages = \App\Message::create($request->all());
        }
        $user = \App\User::find($request["sender_id"]);

        if($user->role === "booster"){
            event(new \App\Events\BoosterSendMessageChat($request->all()));
        }else{
            event(new \App\Events\UserSendMessageChat($request->all()));
        }
        $message = \App\Message::where("id", $messages->id)->with("receiver", "sender")->first();
        return response()->json($message, 200);
    }

    public function createConversation($sender_id, $receiver_id){
        $conversation = Conversation::create(["sender_id"=>$sender_id, "receiver_id"=>$receiver_id]);
        return $conversation;
    }

    public function getConversation($sender_id, $receiver_id){
        $conversation = \App\Conversation::where(["sender_id"=>$sender_id, "receiver_id"=>$receiver_id])
                                            ->orWhere(["sender_id"=>$receiver_id, "receiver_id"=>$sender_id])->first();
                                            return $conversation;
    }
}
