<?php

namespace App\Http\Controllers\Website\Api;

use App\DotaHour;
use App\DotaMMR;
use App\DotaTBD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function index($type = null){

        if($type !== null){
            switch ($type){
                case 1:
                    $accounts = DotaMMR::with("account")->paginate(10);
                    break;
                case 2:
                    $accounts = DotaTBD::with("account")->paginate(10);
                    break;
                case 3:
                    $accounts = DotaHour::with("account")->paginate(10);
                    break;
                default:
                    $accounts = DotaMMR::with("account")->paginate(10);
            }
            return response()->json($accounts, 200, [], JSON_UNESCAPED_UNICODE);
        }

        $accounts = DotaMMR::paginate(10);

        return response()->json($accounts, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
