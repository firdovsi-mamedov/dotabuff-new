<?php

namespace App\Http\Controllers\Website\Api;

use App\Role;
use App\ServiceSolo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{
    //
    public function boosters(){

        $boosters = User::whereHas('roles', function($q){
            $q->where('name', 'booster');
        })->get();

        return response()->json($boosters, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function filter(Request $request){
        $start = $request["start"];
        $end = $request["end"];
        $service_type = $request["service_type"];
        $user_id = $request["user_id"];

        if($start > 0 && $end < 2499){

        }else if($start > 2499 && $end < 2999){

        }else if($start > 2999 && $end < 3499){

        }else if($start > 3499 && $end < 3999){

        }else if($start > 3999 && $end < 4499){

        }else if($start > 4499 && $end < 4999){

        }else if($start > 4999 && $end < 5499){

        }else if($start > 5499 && $end < 5999){

        }

        return response()->json($service, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
