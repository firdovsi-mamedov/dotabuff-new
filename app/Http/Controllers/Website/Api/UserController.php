<?php

namespace App\Http\Controllers\Website\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function index(Request $request){
        $sender_id = $request["sender_id"];
        $conversation_id = $request["conversation_id"];
        $conversation = \App\Conversation::find($conversation_id);

        if($conversation->receiver_id == $sender_id){
            $receiver_id = $conversation->sender_id;
        }else{
            $receiver_id = $conversation->receiver_id;
        }

        return response()->json(\App\User::find($receiver_id), 200);
    }

    public function indexWork(Request $request){
        $sender_id = $request["sender_id"];
        $conversation_id = $request["conversation_id"];
        $conversation = \App\WorkConversation::find($conversation_id);

        if($conversation->receiver_id == $sender_id){
            $receiver_id = $conversation->sender_id;
        }else{
            $receiver_id = $conversation->receiver_id;
        }

        return response()->json(\App\User::find($receiver_id), 200);
    }
}
