<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function index(){
        $user = Auth::user();
        return view('profile.index', compact("user"));
    }
    //
    public function booster(){
        $user = Auth::user();
        return view('profile.booster', compact("user"));
    }
    //
    public function deals(){
        return view('profile.deals');
    }
    //
    public function message($conversation_id = null){
        $checkConversation = \App\Conversation::where([["id", "=", $conversation_id], ["sender_id", "=", Auth::id()]])
                                                ->orWhere([["id", "=", $conversation_id], ["receiver_id", "=", Auth::id()]])
                                                ->count();

        if(!$checkConversation && $conversation_id !== null){
            return abort(404);
        }
        if($conversation_id === null){
            $conversation = $this->getLastConversation();
            if($conversation !== 0){
                $conversation_id = $conversation->id;
                return view('profile.message', compact("conversation_id", "role"));
            }else{
                return view('profile.message', compact("role"));
            }
        }else{
            $this->setMessageAsRead($conversation_id);
        }

        return view('profile.message', compact("conversation_id"));
    }
    //
    public function methods(){
        return view('profile.methods');
    }
    //
    public function plans(){
        return view('profile.plans');
    }
    //
    public function transaction(){
        return view('profile.transaction');
    }
    //
    public function work($conversation_id = null){
        $checkConversation = \App\WorkConversation::where([["id", "=", $conversation_id], ["sender_id", "=", Auth::id()]])
                                                ->orWhere([["id", "=", $conversation_id], ["receiver_id", "=", Auth::id()]])
                                                ->count();

        $user = Auth::user();
        $role = $user->roles[0];

        if(!$checkConversation && $conversation_id !== null){
            return abort(404);
        }
        if($conversation_id === null){
            $conversation = $this->getLastWorkConversation();
            if($conversation !== 0){
                $conversation_id = $conversation->id;
                return view('profile.work', compact("conversation_id", "role"));
            }else{
                return view('profile.work', compact("role"));
            }
        }else{
            $this->setWorkMessageAsRead($conversation_id);
        }

        return view('profile.work', compact("conversation_id", "role"));
    }


    public function getLastConversation(){
        $messages = \App\Message::where("sender_id", Auth::id())->orWhere("receiver_id", Auth::id())->orderBy("id", "DESC")->first();
        if($messages){
            $last = \App\Conversation::find($messages->conversation_id);
            if($last){
                return $last;
            }else{
                $last = 0;
            }
        }else{
            $last = 0;
        }
        return $last;
    }

    public function getLastWorkConversation(){
        $messages = \App\WorkMessage::where("sender_id", Auth::id())->orWhere("receiver_id", Auth::id())->orderBy("id", "DESC")->first();
        if($messages){
            $last = \App\WorkConversation::find($messages->conversation_id);
            if($last){
                return $last;
            }else{
                $last = 0;
            }
        }else{
            $last = 0;
        }
        return $last;
    }

    public function setWorkMessageAsRead($id){
        $messages = \App\WorkMessage::where("conversation_id", $id)->update(["status"=>1]);
    }
    public function setMessageAsRead($id){
        $messages = \App\Message::where("conversation_id", $id)->update(["status"=>1]);
    }
}
