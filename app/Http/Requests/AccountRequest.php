<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"      =>  "required_if:type,2,3",
            "solo"      =>  "required_if:type,1",
            "party"     =>  "required_if:type,1",
            "rank"      =>  "required_if:type,1",
            "reputation"=>  "required_if:type,1",
            "matches"   =>  "required_if:type,1",
            "dotabuff"  =>  "required_if:type,1",
            "stream"    =>  "required_if:type,1",
            "notes"     =>  "required",
            "price"     =>  "required",
            "login"     =>  "required",
            "password"  =>  "required",
            "email"     =>  "required",
            "e_password"=>  "required",
            "description"=> "required",
        ];
    }
}
