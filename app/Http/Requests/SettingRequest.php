<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "dollar"    =>  "sometimes|numeric",
            "euro"      =>  "sometimes|numeric",
            "wmr"       =>  "sometimes|numeric",
            "qiwi"      =>  "sometimes|numeric",
            "visa_rus"  =>  "sometimes|numeric",
            "yandex"    => "sometimes|numeric",
            "paypal"    => "sometimes|numeric"
        ];
    }
}
