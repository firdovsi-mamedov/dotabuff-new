<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "dollar"=>  $this->dollar,
            "euro"  =>  $this->euro,
            "wmr"   =>  $this->wmr,
            "qiwi"  =>  $this->qiwi,
            "visa_rus"  =>  $this->visa_rus,
            "yandex" => $this->yandex,
            "paypal" => $this->paypal
        ];
    }
}
