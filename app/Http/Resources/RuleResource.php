<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"  =>  $this->id,
            "title_ru"  =>  $this->title_ru,
            "title_en"  =>  $this->title_en,
            "content_ru"=>  $this->content_ru,
            "content_en"=>  $this->content_en,
        ];
    }
}
