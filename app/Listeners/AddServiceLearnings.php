<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\ServiceLearning;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AddServiceLearnings implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {

        for($i = 0; $i < 8; $i++){
            ServiceLearning::create([
                "status"=>0,
                "cost_website"=>0,
                "cost_original"=>0,
                "user_id"=>$event->user["id"],
            ]);
        }
        Log::info($event->user["id"]);

    }
}
