<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\ServiceSolo;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AddServiceSolos implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $user_id = $event->user["id"];
        Log::info($user_id);

        for($i = 0; $i < 8; $i++){
            ServiceSolo::create([
                "status"=>0,
                "cost_website"=>0,
                "cost_original"=>0,
                "user_id"=>$user_id,
                "days"=>0
            ]);
        }

    }
}
