<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\ServiceCalibre;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AddServiceCalibres implements ShouldQueue
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {

        for($i = 0; $i < 8; $i++){
            ServiceCalibre::create([
                "status"=>0,
                "cost_website"=>0,
                "cost_original"=>0,
                "user_id"=>$event->user["id"],
                "days"=>0
            ]);
        }
        Log::info($event->user["id"]);

    }
}
