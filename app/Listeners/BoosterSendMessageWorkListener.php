<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoosterSendMessageWorkListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\BoosterSendMessage $event)
    {
        //
        $request = $event->data;

        \App\Jobs\BoosterSendMessageWork::dispatch($request);
    }
}
