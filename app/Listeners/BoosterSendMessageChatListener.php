<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class BoosterSendMessageChatListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\BoosterSendMessageChat $event)
    {
        //
        $request = $event->data;
        Log::info($request);
        \App\Jobs\BoosterSendMessageChat::dispatch($request);
    }
}
