<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DotaTBD extends Model
{
    protected $fillable = ["name", "price", "notes", "status", "login", "password", "email", "e_password", "description"];

    public function account(){
        return $this->hasMany("App\Wishlist", "account_id");
    }
}
