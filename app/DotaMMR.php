<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DotaMMR extends Model
{
    protected $fillable = ["solo", "party", "rank", "reputation", "price", "dotabuff", "matches", "stream", "notes", "status", "login", "password", "email", "e_password", "description"];

    public function account(){
        return $this->hasMany("App\Wishlist", "account_id");
    }
}
