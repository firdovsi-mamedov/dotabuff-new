<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceLearning extends Model
{
    //
    protected $fillable = ["status", "cost_website", "cost_original", "user_id"];
}
